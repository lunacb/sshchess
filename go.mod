module codeberg.org/lunacb/sshchess

go 1.12

require (
	github.com/gdamore/tcell/v2 v2.5.3
	github.com/mattn/go-sqlite3 v1.14.15
	github.com/pborman/getopt/v2 v2.1.0
	golang.org/x/crypto v0.0.0-20220829220503-c86fa9a7ed90
)
