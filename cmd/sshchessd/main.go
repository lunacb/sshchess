package main

import (
	"fmt"
	"github.com/pborman/getopt/v2"
	"io"
	"io/ioutil"
	"os"
)

type runOpts struct {
	sockpath, dbpath string
	logInfo, logErr io.Writer
}

func main() {
	usage := false
	logfile := ""
	quiet := false
	opts := runOpts{
		sockpath: "/tmp/sshchess.sock",
		dbpath: "/var/sshchess/db.sql",
		logInfo: nil,
		logErr: nil,
	}

	getopt.FlagLong(&usage, "help", 'h', "Print this message")
	getopt.FlagLong(&opts.sockpath, "socket", 's', "Path to the server socket")
	getopt.FlagLong(&opts.dbpath, "database", 'd', "Path to the server database")
	getopt.FlagLong(&logfile, "logfile", 'o', "File to log messages to")
	getopt.FlagLong(&quiet, "quiet", 'q', "Supress info messages")
	getopt.Parse()

	if usage {
		getopt.Usage()
		os.Exit(1)
	}

	log := os.Stdout
	if getopt.IsSet('o') {
		var err error
		if log, err = os.OpenFile(logfile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644); err != nil {
			fmt.Fprintf(os.Stderr, "Failed to open file %s: %s\n", logfile, err)
			os.Exit(1)
		}
	}

	opts.logErr = log
	if quiet {
		opts.logInfo = ioutil.Discard
	} else {
		opts.logInfo = log
	}

	exit := run(opts)

	log.Close()

	os.Exit(exit)
}
