package main

import (
	"bufio"
	"database/sql"
	"io"
	"log"
	"math/rand"
	"net"
	"os"
	"os/signal"
	"path"
	"sort"
	"strconv"
	"strings"
	"syscall"
	"time"

	"codeberg.org/lunacb/sshchess/lib/board"
	"codeberg.org/lunacb/sshchess/lib/consts"
	"codeberg.org/lunacb/sshchess/cmd/sshchessd/db"
	"codeberg.org/lunacb/sshchess/cmd/sshchessd/protocol"
)

type matchState int

const (
	matchPlaying matchState = iota
	matchLeaving
	matchRematching
)

type watcher struct {
	userPtr *user
	next, prev *watcher
}

type match struct {
	broadcastMsg string

	state matchState
	waiting int

	board board.Board

	current, other *user

	watchers watcher
}

type userMode int

const (
	modeIncomplete userMode = iota
	modeNormal
	modeWaiting
	modePlaying
	modeWatching
	modeRematching
)

const (
	hiscoreLength = 10
)


type user struct {
	conn net.Conn
	ok chan bool

	mode userMode

	name string
	guest bool

	wins, losses, weeklyWins, weeklyLosses int

	// If userMode is modeWaiting, nil if waiting for a random match, otherwise
	// the user being challenged.
	challenging *user

	matchPtr *match
	matchTeam board.GameSide

	watching watcher
}

type hiscoreEntry struct {
	name string
	guest bool
	wins, losses int

	// will be NULL if not logged in
	userPtr *user
}

type anyEvent interface {}

type sockEvent struct {
	userPtr *user
	eventid string
	event protocol.Msg
}

type sockErr struct {
	err error
	userPtr *user
}

// Global Variables

var hiscore []hiscoreEntry
var hiscoreWeekly []hiscoreEntry
var users []*user
var matches []*match
var srvdb *sql.DB
var logInfo, logErr *log.Logger

func (u *user) SaveToDb() error {
	return db.UpdateUser(srvdb, u.name, u.wins, u.losses, u.weeklyWins, u.weeklyLosses)
}

func hiscoreUserJoin(u *user, scores []hiscoreEntry) {
	for i, v := range scores {
		if v.name == u.name {
			scores[i].userPtr = u
		}
	}
}

func hiscoreUserLeave(u *user, scores []hiscoreEntry) {
	for i, v := range scores {
		if v.name == u.name {
			scores[i].userPtr = nil
		}
	}
}

func send(userPtr *user, params ...string) {
	io.WriteString(userPtr.conn, strings.Join(params, " ") + "\n")
}

func logName(userPtr *user) string {
	res := ""
	if userPtr.mode == modeIncomplete {
		res = "(not logged in)"
	} else {
		res = userPtr.name
		if userPtr.guest {
			res += " (guest)"
		} else {
			res += " (user)"
		}
	}

	return res
}

func sendOk(event sockEvent) {
	send(event.userPtr, consts.ServerOk, event.eventid)

	msg, send := event.event.Log()
	if send {
		logInfo.Printf("%s: ok: %s\n", logName(event.userPtr), msg)
	}
}

func msgErr(event sockEvent, errstr string, bad bool) bool {
	if bad {
		send(event.userPtr, consts.ServerErr, errstr, event.eventid)
		msg, send := event.event.Log()
		if send {
			logErr.Printf("%s: %s: %s\n", logName(event.userPtr), errstr, msg)
		}
		event.userPtr.ok <-true
	}

	return bad
}


func hiscoreNotifyUser(u *user, board string, scores []hiscoreEntry) {
	if u.mode != modeIncomplete {
		io.WriteString(u.conn, consts.ServerLeaderboard + " " + board)

		for _, v := range scores {
			var guest string
			if v.guest {
				guest = "guest"
			} else {
				guest = "user"
			}
			io.WriteString(u.conn, " " + strings.Join([]string{guest, v.name,
				strconv.Itoa(v.wins),  strconv.Itoa(v.losses)}, " "))
		}
		io.WriteString(u.conn, "\n")
	}
}

func hiscoreNotifyAll(board string, scores []hiscoreEntry) {
	for _, u := range users {
		hiscoreNotifyUser(u, board, scores)
	}
}

func hiscoreUpdate(u *user, wins int, losses int, scoresPtr *[]hiscoreEntry) bool {
	// You're not on the leaderboard if you've never won.
	if wins == 0 {
		return false
	}

	scores := *scoresPtr

	newentry := hiscoreEntry{
		name: u.name,
		guest: u.guest,
		wins: wins,
		losses: losses,
		userPtr: u,
	}

	// If we can find the user already in the hiscore, move it up as needed.
	// NOTE: This assumes that the score can only have increased from what it
	// was previously.
	for i, v := range scores {
		if v.userPtr != u { continue }

		pos := i

		for j := pos - 1; j >= 0 && wins > scores[j].wins; j -= 1 {
			scores[pos] = scores[j]
			pos -= 1
		}

		scores[pos] = newentry

		return true
	}

	// Try to insert the user somewhere in the list.
	for i, v := range scores {
		if wins > v.wins {
			if len(scores) < hiscoreLength {
				// Make room for the entries to be pushed back, if possible.
				*scoresPtr = append(scores, hiscoreEntry{})
			}
			for j := len(scores) - 2; j >= i; j-- {
				scores[j+1] = scores[j]
			}
			scores[i] = newentry

			return true
		}
	}

	// Try to append the user to the end of the list.
	if len(scores) < hiscoreLength {
		*scoresPtr = append(scores, newentry)
		return true
	}

	return false
}

func matchUserQuit(ptr *user) {
	matchPtr := ptr.matchPtr
	if (ptr.mode != modePlaying && ptr.mode != modeRematching) || matchPtr == nil { return }
	ptr.mode = modeNormal

	var notme *user = nil
	if matchPtr.current == ptr {
		matchPtr.current = nil
		notme = matchPtr.other
	} else if matchPtr.other == ptr {
		matchPtr.other = nil
		notme = matchPtr.current
	} else {
		panic("BUG")
	}

	if notme != nil {
		for _, u := range users {
			if u.mode != modeIncomplete {
				send(u, consts.ServerRmMatch, matchPtr.broadcastMsg)
			}
		}

		send(notme, consts.ServerGameAlone)
	}

	switch matchPtr.state {
	case matchPlaying:
		matchPtr.state = matchLeaving
		matchPtr.waiting = 1
	case matchLeaving:
		matchPtr.waiting -= 1
		if matchPtr.waiting == 0 {
			for pos := matchPtr.watchers.next; pos != &matchPtr.watchers; {
				next := pos.next

				send(pos.userPtr, consts.ServerWatchDestroy)
				pos.userPtr.mode = modeNormal
				pos.userPtr.watching.next = nil
				pos.userPtr.watching.prev = nil

				pos = next
			}
			for i, m := range matches {
				if m == matchPtr {
					matches[i] = matches[len(matches)-1]
					matches = matches[:len(matches)-1]
					return
				}
			}
		}
	case matchRematching:
		matchPtr.state = matchLeaving
		matchPtr.waiting = 1
	}
}

func removeUser(ptr *user) {
	matchUserQuit(ptr)

	if ptr.mode == modeWatching {
		ptr.watching.prev.next = ptr.watching.next
		ptr.watching.next.prev = ptr.watching.prev
		ptr.watching.prev = nil
		ptr.watching.next = nil
	}

	for _, u := range users {
		if u.mode == modeWaiting && u.challenging == ptr {
			send(u, consts.ServerWaitingQuit)
			u.mode = modeNormal
		}
	}

	for i, u := range users {
		if u == ptr {
			users[i] = users[len(users)-1]
			users = users[:len(users)-1]
			return
		}
	}

	hiscoreUserLeave(ptr, hiscore)
	hiscoreUserLeave(ptr, hiscoreWeekly)
}

func findUser(name string, guest bool) (*user, bool) {
	for _, u := range users {
		if u.name == name && u.guest == guest { return u, true }
	}
	return nil, false
}

func strColor(userPtr *user) string {
	if userPtr.matchTeam == board.TWhite {
		return "white"
	} else {
		return "black"
	}
}

func strName(userPtr *user) string {
	if userPtr.guest {
		return "guest " + userPtr.name
	} else {
		return "user " + userPtr.name
	}
}

func strNameStat(userPtr *user) string  {
	if userPtr.guest {
		return "guest " + userPtr.name + " 0 0"
	} else {
		return strings.Join([]string{"user", userPtr.name,
			strconv.Itoa(userPtr.wins), strconv.Itoa(userPtr.losses)}, " ")
	}
}

func writeWatchGame(userPtr *user, matchPtr *match) {
	send(userPtr, consts.ServerWatchBegin, strName(matchPtr.current),
		strColor(matchPtr.current), strName(matchPtr.other),
		strColor(matchPtr.other), matchPtr.board.Msg())
}

func startMatch(matchPtr *match, white *user, black *user, rematch bool) {
	if rand.Int() % 2 == 0 {
		white, black = black, white
	}

	watchers := watcher{
		userPtr: nil,
		next: &matchPtr.watchers,
		prev: &matchPtr.watchers,
	}
	broadcastMsg := matchPtr.broadcastMsg

	if rematch {
		watchers = matchPtr.watchers
	}

	*matchPtr = match{
		board: board.NewBoard(),
		state: matchPlaying,
		waiting: 0,
		current: white,
		other: black,
		watchers: watchers,
		// set later on
		broadcastMsg: "",
	}

	white.matchPtr = matchPtr
	white.matchTeam = board.TWhite
	white.mode = modePlaying
	black.matchPtr = matchPtr
	black.matchTeam = board.TBlack
	black.mode = modePlaying
	send(white, consts.ServerGameBegin, strColor(white), strName(black))
	send(black, consts.ServerGameBegin, strColor(black), strName(white))

	logInfo.Printf("match %s %s: begin\n", logName(white), logName(black))

	send(white, consts.ServerGameTurn)

	for pos := matchPtr.watchers.next; pos != &matchPtr.watchers; pos = pos.next {
		writeWatchGame(pos.userPtr, matchPtr)
	}

	if !rematch {
		matchPtr.broadcastMsg = strName(white) + " " + strName(black)

		for _, u := range users {
			if u.mode != modeIncomplete {
				send(u, consts.ServerAddMatch, matchPtr.broadcastMsg)
			}
		}
	} else {
		matchPtr.broadcastMsg = broadcastMsg
	}

}

func pullTop(scores []hiscoreEntry, alltime bool) ([]hiscoreEntry, error) {
	dbHighscore, err := db.TopScores(srvdb, hiscoreLength, alltime)
	if err != nil {
		return scores, err
	} else {
		for _, v := range dbHighscore {
			scores = append(scores, hiscoreEntry{
				name: v.Name,
				wins: v.Wins,
				guest: false,
				losses: v.Losses,
				userPtr: nil,
			})
		}
	}
	return scores, nil
}

func updateUserScore(userPtr *user, winInc int, lossInc int, updatePtr *bool, weeklyUpdatePtr *bool) error {
	if userPtr.guest { return nil }

	userPtr.wins += winInc
	userPtr.weeklyWins += winInc
	userPtr.losses += lossInc
	userPtr.weeklyLosses += lossInc

	updated := hiscoreUpdate(userPtr, userPtr.wins,
		userPtr.losses, &hiscore)
	weeklyUpdated := hiscoreUpdate(userPtr, userPtr.weeklyWins,
		userPtr.weeklyLosses, &hiscoreWeekly)

	if updated { *updatePtr = true }
	if weeklyUpdated { *weeklyUpdatePtr = true }

	if err := userPtr.SaveToDb(); err != nil {
		return err
	}

	return nil
}

func run(opts runOpts) int {
	var err error

	rand.Seed(time.Now().UnixNano())

	logInfo = log.New(opts.logInfo, "INFO: ", log.Ldate | log.Ltime | log.Lmsgprefix)
	logErr = log.New(opts.logErr, "ERROR: ", log.Ldate | log.Ltime | log.Lmsgprefix)

	if err := os.MkdirAll(path.Dir(opts.sockpath), 0755); err != nil && !os.IsExist(err) {
		logErr.Printf("Failed to create directory for %s: %s\n", opts.sockpath, err)
		return 1
	}
	if err := os.MkdirAll(path.Dir(opts.dbpath), 0755); err != nil && !os.IsExist(err) {
		logErr.Printf("Failed to create directory for %s: %s\n", opts.dbpath, err)
		return 1
	}

	hiscore = make([]hiscoreEntry, 0, hiscoreLength)
	hiscoreWeekly = make([]hiscoreEntry, 0, hiscoreLength)

	users = []*user{}
	matches = []*match{}

	var weekstart time.Time
	{
		now := time.Now()
		weekday := now.Weekday()
		sunday := now.AddDate(0, 0, int(time.Sunday - weekday))
		year, month, day := sunday.Date()
		weekstart = time.Date(year, month, day, 0, 0, 0, 0, time.Local)
	}

	if srvdb, err = db.Open(opts.dbpath, weekstart); err != nil {
		logErr.Printf("Failed to open database %s: %s\n", opts.dbpath, err)
		return 1
	}

	defer srvdb.Close()

	if hiscore, err = pullTop(hiscore, true); err != nil {
		logErr.Printf("Failed to load allitme highscores: %s\n", err)
		return 1
	}
	if hiscoreWeekly, err = pullTop(hiscoreWeekly, false); err != nil {
		logErr.Printf("Failed to load weekly highscores: %s\n", err)
		return 1
	}

	sockEventCh := make(chan sockEvent)
	sockErrCh := make(chan sockErr)
	sockRegisterCh := make(chan *user)
	timerCh := make(chan time.Time)
	fatalCh := make(chan error)
	signalCh := make(chan os.Signal, 1)

	signal.Notify(signalCh, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	// If not nil, specifies a user waiting for a match.
	var waitingUser *user = nil

	go func() {
		t := weekstart

		ticker := time.NewTicker(time.Minute)


		for {
			// Wait for next week.
			t = t.AddDate(0, 0, 7)

			// Wake up every minute to see of we're close to a week passing.
			for {
				now := <-ticker.C
				diff := t.Sub(now)
				if diff <= time.Minute {
					time.Sleep(diff)
					break
				}
			}

			timerCh <-t
		}
	}()

	var ln net.Listener

	ln, err = net.Listen("unix", opts.sockpath)
	if err != nil {
		logErr.Printf("Failed to listen on %s: %s\n", opts.sockpath, err)
		return 1
	}

	defer func() {
		for _, u := range users {
			send(u, consts.ServerTerm)
		}
		ln.Close()
	}()

	if err := os.Chmod(opts.sockpath, 0777); err != nil {
		logErr.Printf("Failed to change socket permissions: %s\n", err)
		return 1
	}

	go func() {
		for {
			conn, err := ln.Accept()
			if err != nil {
				fatalCh <-err
				return
			}

			go func() {
				defer conn.Close()

				reader := bufio.NewReader(conn)
				ok := make(chan bool)
				userPtr := new(user)
				*userPtr = user{
					conn: conn,
					ok: ok,
					mode: modeIncomplete,

					name: "",
					guest: false,

					wins: 0,
					losses: 0,

					weeklyWins: 0,
					weeklyLosses: 0,

					challenging: nil,
					matchPtr: nil,
					matchTeam: board.TWhite,
				}

				sockRegisterCh <-userPtr

				for {
					line, err := reader.ReadString('\n')
					if err != nil {
						sockErrCh <-sockErr{
							err: err,
							userPtr: userPtr,
						}
						return
					}

					if line[len(line)-1] == '\n' { line = line[:len(line)-1] }

					event, eventid := protocol.ParseLine(line)

					sockEventCh <-sockEvent{
						userPtr: userPtr,
						event: event,
						eventid: eventid,
					}

					if <-ok == false {
						return
					}

				}
			}()
		}
	}()

	for {
		select {
		case e := <-sockErrCh:
			if e.err != io.EOF {
				logErr.Printf("Connection error: %s\n", e.err)
			}
			removeUser(e.userPtr)
			if e.userPtr.mode == modeWaiting && e.userPtr.challenging != nil {
				send(e.userPtr.challenging, consts.ServerRmChallenge, strName(e.userPtr))
			}
			if waitingUser == e.userPtr {
				waitingUser = nil
			}

		case e := <-sockEventCh:
			switch msg := e.event.(type) {

			case protocol.MsgSendVersion:
				sendOk(e)
				send(e.userPtr, consts.ServerVersion, strconv.Itoa(consts.Major),
					strconv.Itoa(consts.Minor), strconv.Itoa(consts.Patch))

				e.userPtr.ok <-true

			case protocol.MsgTerm:
				sendOk(e)
				send(e.userPtr, consts.ServerTerm)
				removeUser(e.userPtr)
				if e.userPtr.mode == modeWaiting && e.userPtr.challenging != nil {
					send(e.userPtr.challenging, consts.ServerRmChallenge, strName(e.userPtr))
				}
				if waitingUser == e.userPtr {
					waitingUser = nil
				}

				e.userPtr.ok <-false

			case protocol.MsgInvalid:
				send(e.userPtr, consts.ServerErr, "invalid", e.eventid)
				logmsg, send := e.event.Log()
				if send {
					logInfo.Printf("%s: %s: %s\n", logName(e.userPtr), "invalid", logmsg)
				}

				e.userPtr.ok <-true

			case protocol.MsgLogin:
				if msgErr(e, "ctx", (e.userPtr.mode != modeIncomplete)) { break }

				if msg.Guest {
					exists := false
					for _, v := range users {
						if v.guest && v.name == msg.Username {
							exists = true
							break
						}
					}
					if msgErr(e, "exists", exists) { break }
				} else {
					good, info, err := db.AuthGetUser(srvdb, msg.Username, msg.Password)
					if !good {
						if err != nil {
							logErr.Printf("Failed to get database user entry: %s\n", err)

							msgErr(e, "server", true)
						} else {
							msgErr(e, "account", true)
						}
						break
					}
					exists := false
					for _, v := range users {
						if !v.guest && v.name == msg.Username {
							exists = true
							break
						}
					}
					if msgErr(e, "exists", exists) { break }

					e.userPtr.wins = info.Wins
					e.userPtr.losses = info.Losses
					e.userPtr.weeklyWins = info.WeeklyWins
					e.userPtr.weeklyLosses = info.WeeklyLosses
				}

				sendOk(e)

				e.userPtr.name = msg.Username
				e.userPtr.guest = msg.Guest
				e.userPtr.mode = modeNormal

				hiscoreNotifyUser(e.userPtr, "alltime", hiscore)
				hiscoreNotifyUser(e.userPtr, "weekly", hiscoreWeekly)
				for _, v := range matches {
					send(e.userPtr, consts.ServerAddMatch, v.broadcastMsg)
				}

				if !msg.Guest {
					hiscoreUserJoin(e.userPtr, hiscore)
					hiscoreUserJoin(e.userPtr, hiscoreWeekly)
				}

				e.userPtr.ok <-true

			case protocol.MsgCreateAccount:
				if msgErr(e, "ctx", (e.userPtr.mode != modeIncomplete)) { break }

				good, err := db.NewUser(srvdb, msg.Username, msg.Password)

				if !good {
					if err != nil {
						logErr.Printf("Failed to create database user entry: %s\n", err)
						msgErr(e, "server", true)
					} else {
						msgErr(e, "exists", true)
					}
					break
				}

				sendOk(e)

				e.userPtr.name = msg.Username
				e.userPtr.guest = false
				e.userPtr.mode = modeNormal

				hiscoreNotifyUser(e.userPtr, "alltime", hiscore)
				hiscoreNotifyUser(e.userPtr, "weekly", hiscoreWeekly)

				e.userPtr.ok <-true

			case protocol.MsgRandomMatch:
				if msgErr(e, "ctx", (e.userPtr.mode != modeNormal)) { break }
				sendOk(e)

				e.userPtr.mode = modeWaiting
				e.userPtr.challenging = nil

				if waitingUser != nil {
					matchPtr := new(match)
					matches = append(matches, matchPtr)
					startMatch(matchPtr, waitingUser, e.userPtr, false)
					waitingUser = nil
				} else {
					waitingUser = e.userPtr
				}

				e.userPtr.ok <-true

			case protocol.MsgChallengeUser:
				if msgErr(e, "ctx", (e.userPtr.mode != modeNormal)) { break }

				var good bool
				var userPtr *user
				userPtr, good = findUser(msg.Name, msg.Guest);
				if msgErr(e, "nomatch", (!good)) { break }
				if msgErr(e, "ctx", (userPtr == e.userPtr)) { break }
				challenged := (userPtr.mode == modeWaiting && userPtr.challenging == e.userPtr)
				if msgErr(e, "ctx", (!challenged && msg.Accepting)) { break }
				sendOk(e)

				if challenged {
					// The user already challenged us first, start the game.

					matchPtr := new(match)
					matches = append(matches, matchPtr)
					startMatch(matchPtr, e.userPtr, userPtr, false)
				} else {
					e.userPtr.mode = modeWaiting
					e.userPtr.challenging = userPtr

					send(userPtr, consts.ServerAddChallenge, strName(e.userPtr))
				}

				e.userPtr.ok <-true

			case protocol.MsgWatchMatch:
				if msgErr(e, "ctx", (e.userPtr.mode != modeNormal)) { break }

				var firstGood, secondGood bool
				var first, second *user

				first, firstGood = findUser(msg.NameFirst, msg.GuestFirst)
				second, secondGood = findUser(msg.NameSecond, msg.GuestSecond)

				if msgErr(e, "nomatch", (first == second || !firstGood || !secondGood)) { break }

				var matchPtr *match = nil
				for _, m := range matches {
					if (first == m.current || first == m.other) && (second == m.current || second == m.other) {
						matchPtr = m
						break
					}
				}

				if msgErr(e, "nomatch", (matchPtr == nil)) { break }

				sendOk(e)

				// Insert watcher linked list item.
				e.userPtr.watching = watcher{
					userPtr: e.userPtr,
					next: matchPtr.watchers.next,
					prev: &matchPtr.watchers,
				}
				matchPtr.watchers.next.prev = &e.userPtr.watching
				matchPtr.watchers.next = &e.userPtr.watching
				e.userPtr.mode = modeWatching

				writeWatchGame(e.userPtr, matchPtr)
				if matchPtr.state != matchPlaying {
					send(e.userPtr, consts.ServerWatchEnd, strColor(matchPtr.current))
				}

				e.userPtr.ok <-true

			case protocol.MsgWatchQuit:
				e.userPtr.ok <-true

				if msgErr(e, "ctx", (e.userPtr.mode != modeWatching)) { break }
				sendOk(e)

				e.userPtr.watching.prev.next = e.userPtr.watching.next
				e.userPtr.watching.next.prev = e.userPtr.watching.prev
				e.userPtr.watching.prev = nil
				e.userPtr.watching.next = nil
				e.userPtr.mode = modeNormal

			case protocol.MsgWaitingQuit:
				if msgErr(e, "ctx", (e.userPtr.mode != modeWaiting)) { break }
				sendOk(e)

				if e.userPtr.challenging != nil {
					send(e.userPtr.challenging, consts.ServerRmChallenge, strName(e.userPtr))
				}
				if waitingUser == e.userPtr {
					waitingUser = nil
				}
				e.userPtr.mode = modeNormal

				e.userPtr.ok <-true

			case protocol.MsgSearchUser:
				sendOk(e)

				truncated := false
				sent := 0

				io.WriteString(e.userPtr.conn, consts.ServerSearch)

				// TODO: Sort as users join/leave so I don't have to do it here.
				userslice := make([]*user, 0, len(users))
				for _, u := range users {
					if !strings.Contains(u.name, msg.Search) { continue }
					userslice = append(userslice, u)
				}
				sort.Slice(userslice, func(i, j int) bool { return userslice[i].name < userslice[j].name })

				// Send only exact online matches.
				{
					i := sort.Search(len(userslice), func(i int) bool {
						return strings.Compare(msg.Search, userslice[i].name) >= 0
					})
					for ; i < len(userslice) && userslice[i].name == msg.Search; i++ {
						io.WriteString(e.userPtr.conn, " online " + strNameStat(userslice[i]))
						sent += 1
					}
				}

				// Send all other online matches.
				for _, u := range userslice {
					if u.name == msg.Search { continue }

					if sent >= db.MaxResults {
						truncated = true
						break
					}

					sent += 1
					io.WriteString(e.userPtr.conn, " online " + strNameStat(u))
				}

				if msg.Offline {
					// Send all offline users.

					if(!truncated) {
						var err error

						truncated, err = db.SearchUsers(srvdb, msg.Search,
								&sent, func(name string, wins int, losses int) bool {
							i := sort.Search(len(userslice), func(i int) bool {
								return strings.Compare(name, userslice[i].name) >= 0
							})

							if i < len(userslice) { return false }
							io.WriteString(e.userPtr.conn, " " +
								strings.Join([]string{"offline", "user", name,
								strconv.Itoa(wins), strconv.Itoa(losses)}, " "))

							return true
						})

						if err != nil {
							logErr.Printf("Failed to search database: %s\n", err)
						}
					}
				}

				if truncated {
					io.WriteString(e.userPtr.conn, " trunc\n")
				} else {
					io.WriteString(e.userPtr.conn, "\n")
				}

				e.userPtr.ok <-true

			case protocol.MsgGameMove:
				matchPtr := e.userPtr.matchPtr
				if msgErr(e, "ctx",
					(e.userPtr.mode != modePlaying || matchPtr.state != matchPlaying ||
					matchPtr.current != e.userPtr)) { break }

				move, good := matchPtr.board.TryMove(e.userPtr.matchTeam, msg)
				if msgErr(e, "bad", (!good)) { break }

				sendOk(e)

				send(matchPtr.other, consts.ServerGameMove, move)
				for pos := matchPtr.watchers.next; pos != &matchPtr.watchers; pos = pos.next {
					send(pos.userPtr, consts.ServerWatchMove, move)
				}

				win := matchPtr.board.Checkmated(!e.userPtr.matchTeam)
				if win == board.WinNone {
					// Opponent's turn.

					tmp := matchPtr.current
					matchPtr.current = matchPtr.other
					matchPtr.other = tmp
					send(matchPtr.current, consts.ServerGameTurn)

					for pos := matchPtr.watchers.next; pos != &matchPtr.watchers; pos = pos.next {
						send(pos.userPtr, consts.ServerWatchTurn, strColor(matchPtr.current))
					}
				} else {
					// User won.

					// Update wins and losses.
					updated := false
					weeklyUpdated := false
					if err := updateUserScore(matchPtr.other, 0, 1, &updated, &weeklyUpdated); err != nil {
						logErr.Printf("Failed to save user score: %s\n", err)
					}
					if err := updateUserScore(matchPtr.current, 1, 0, &updated, &weeklyUpdated); err != nil {
						logErr.Printf("Failed to save user score: %s\n", err)
					}
					if updated {
						hiscoreNotifyAll("alltime", hiscore)
					}
					if weeklyUpdated {
						hiscoreNotifyAll("weekly", hiscoreWeekly)
					}

					send(matchPtr.current, consts.ServerGameEnd, strColor(matchPtr.current))
					send(matchPtr.other, consts.ServerGameEnd, strColor(matchPtr.current))
					if matchPtr.current.matchTeam == board.TWhite {
					logInfo.Printf("match %s %s: %s won\n", logName(matchPtr.current),
						logName(matchPtr.other), logName(matchPtr.current))
					} else {
					logInfo.Printf("match %s %s: %s won\n", logName(matchPtr.other),
						logName(matchPtr.current), logName(matchPtr.current))
					}

					for pos := matchPtr.watchers.next; pos != &matchPtr.watchers; pos = pos.next {
						send(pos.userPtr, consts.ServerWatchEnd, strColor(matchPtr.current))
					}

					matchPtr.state = matchLeaving
					matchPtr.waiting = 2
				}

				e.userPtr.ok <-true

			case protocol.MsgGameQuit:
				if msgErr(e, "ctx", (e.userPtr.mode != modePlaying &&
					e.userPtr.mode != modeRematching)) { break }

				matchUserQuit(e.userPtr)

				sendOk(e)

				e.userPtr.ok <-true

			case protocol.MsgGameRematch:
				matchPtr := e.userPtr.matchPtr
				if msgErr(e, "ctx", (e.userPtr.mode != modePlaying ||
					matchPtr.state == matchPlaying ||
					(matchPtr.state == matchLeaving && matchPtr.waiting == 1))) { break }
				sendOk(e)

				e.userPtr.mode = modeRematching

				if matchPtr.state == matchLeaving {
					// Send ServerGameWantRematch if the other user hasn't
					// requested a rematch already.
					if matchPtr.current == e.userPtr {
						send(matchPtr.other, consts.ServerGameWantRematch)
					} else {
						send(matchPtr.current, consts.ServerGameWantRematch)
					}

					matchPtr.state = matchRematching
					matchPtr.waiting = 1
				} else if matchPtr.state == matchRematching  {
					matchPtr.waiting -= 1
					if matchPtr.waiting == 0 {
						startMatch(matchPtr, matchPtr.current, matchPtr.other, true)
					}
				} else {
					panic("BUG")
				}

				e.userPtr.ok <-true

			default: panic("Unknown event")
			}

		case e := <-fatalCh:
			log.Fatal(e)

		case e := <-sockRegisterCh:
			users = append(users, e)

		case e := <-timerCh:
			for i, _ := range users {
				users[i].weeklyWins = 0
				users[i].weeklyLosses = 0
			}
			db.ResetWeekly(srvdb, e)

			hiscoreWeekly = hiscoreWeekly[:0]
			hiscoreNotifyAll("weekly", hiscoreWeekly)

		case <-signalCh:
			return 0
		}
	}
}
