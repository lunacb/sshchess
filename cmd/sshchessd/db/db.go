package db

import (
	"database/sql"
	"errors"
	"golang.org/x/crypto/bcrypt"
	"time"
	_ "github.com/mattn/go-sqlite3"

	"codeberg.org/lunacb/sshchess/lib/consts"
)

type UserInfo struct {
	Wins int
	Losses int
	WeeklyWins int
	WeeklyLosses int
}

type TopInfo struct {
	Name string
	Wins int
	Losses int
}

const (
	MaxResults = 100
)

func Open(path string, weekstart time.Time) (*sql.DB, error) {
	var query string

	db, err := sql.Open("sqlite3", path)
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		db.Close()
		return nil, err
	}

	query = `SELECT major, minor, patch FROM meta`
	var major, minor, patch int
	if err := db.QueryRow(query).Scan(&major, &minor, &patch); err != nil {
		// Assume the database needs to be created.

		query = `DROP TABLE IF EXISTS users`
		if _, err := db.Exec(query); err != nil {
			db.Close()
			return nil, err
		}

		query = `DROP TABLE IF EXISTS meta`
		if _, err := db.Exec(query); err != nil {
			db.Close()
			return nil, err
		}

		query =
			`CREATE TABLE IF NOT EXISTS users (
				name TEXT NOT NULL,
				pass TEXT NOT NULL,
				wins INTEGER NOT NULL,
				losses INTEGER NOT NULL,
				weekly_wins INTEGER NOT NULL,
				weekly_losses INTEGER NOT NULL
			)`
		if _, err := db.Exec(query); err != nil {
			db.Close()
			return nil, err
		}

		query =
			`CREATE TABLE IF NOT EXISTS meta (
				weekly STRING NOT NULL,
				major INTEGER NOT NULL,
				minor INTEGER NOT NULL,
				patch INTEGER NOT NULL
			)`
		if _, err := db.Exec(query); err != nil {
			db.Close()
			return nil, err
		}

		epoch := time.Unix(0, 0).Format(time.RFC3339)
		query = `INSERT INTO meta (weekly, major, minor, patch) VALUES ($1, $2, $3, $4)`
		if _, err := db.Exec(query, epoch, consts.Major, consts.Minor, consts.Patch); err != nil {
			db.Close()
			return nil, err
		}
	} else {
		if major != consts.Major || minor != consts.Minor {
			db.Close()
			return nil, errors.New("incompatible version number")
		}
	}

	query = `SELECT weekly FROM meta`
	var weekly string
	resetweekly := true
	if err := db.QueryRow(query).Scan(&weekly); err != nil {
		db.Close()
		return nil, err
	}

	t, err := time.Parse(time.RFC3339, weekly)
	if err != nil {
		db.Close()
		return nil, err
	}

	if time.Since(t) < time.Hour * 24 * 7 {
		resetweekly = false
	}

	if resetweekly {
		if err := ResetWeekly(db, weekstart); err != nil {
			db.Close()
			return nil, err
		}
	}

	return db, nil
}

func AuthGetUser(db *sql.DB, name string, pass string) (bool, UserInfo, error) {
	var hash string
	var info UserInfo

	query := `SELECT pass, wins, losses, weekly_wins, weekly_losses FROM users WHERE name = $1`
	if err := db.QueryRow(query, name).Scan(&hash, &info.Wins, &info.Losses, &info.WeeklyWins, &info.WeeklyLosses); err != nil {
		if err == sql.ErrNoRows { err = nil }
		return false, UserInfo{}, err
	}

	if bcrypt.CompareHashAndPassword([]byte(hash), []byte(pass)) != nil {
		return false, UserInfo{}, nil
	}

	return true, info, nil
}

func NewUser(db *sql.DB, name string, pass string) (bool, error) {
	var exists int

	query := `SELECT EXISTS(SELECT * FROM users WHERE name = $1)`
	if err := db.QueryRow(query, name).Scan(&exists); err != nil {
		return false, err
	}
	if exists == 1 {
		return false, nil
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(pass), bcrypt.DefaultCost)
	if err != nil {
		return false, err
	}

	query = `INSERT INTO users (name, pass, wins, losses, weekly_wins, weekly_losses) VALUES ($1, $2, $3, $4, $5, $6)`
	if _, err := db.Exec(query, name, string(hash), 0, 0, 0, 0); err != nil {
		return false, err
	}

	return true, nil
}

func UpdateUser(db *sql.DB, name string, wins int, losses int, weeklyWins int, weeklyLosses int) error {
	query := `UPDATE users SET wins = $1, losses = $2, weekly_wins = $3, weekly_losses = $4 WHERE name = $5`
	if _, err := db.Exec(query, wins, losses, weeklyWins, weeklyLosses, name); err != nil {
		return err
	}

	return nil
}

type SearchRunFunc func(name string, wins int, losses int) bool

func SearchUsers(db *sql.DB, search string, count *int, run SearchRunFunc) (bool, error) {
	query :=
		`SELECT name, wins, losses FROM users
		WHERE instr(name, $1) != 0
		ORDER BY name != $1, name
		LIMIT $2`
	rows, err := db.Query(query, search, MaxResults + 1)
	if err != nil {
		return false, err
	}
	defer rows.Close()

	truncated := false
	for rows.Next() {
		if *count >= MaxResults {
			truncated = true
			break
		}

		var name string
		var wins, losses int
		if err := rows.Scan(&name, &wins, &losses); err != nil {
			return truncated, err
		}
		if run(name, wins, losses) {
			*count += 1
		}
	}
	if err := rows.Err(); err != nil {
		return truncated, err
	}

	return truncated, nil
}

func top(db *sql.DB, max int, query string) ([]TopInfo, error) {
	res := make([]TopInfo, 0, max)

	rows, err := db.Query(query, max)
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		item := TopInfo{}
		if err := rows.Scan(&item.Name, &item.Wins, &item.Losses); err != nil {
			return res, err
		}
		res = append(res, item)
	}
	if err := rows.Err(); err != nil {
		return res, err
	}

	return res, nil
}

func TopScores(db *sql.DB, max int, alltime bool) ([]TopInfo, error) {
	var query string
	if alltime {
		query =
			`SELECT name, wins, losses FROM users
			WHERE wins > 0
			ORDER BY wins DESC
			LIMIT $1`
	} else {
		query =
			`SELECT name, weekly_wins, weekly_losses FROM users
			WHERE weekly_wins > 0
			ORDER BY weekly_wins DESC
			LIMIT $1`
	}
	return top(db, max, query)
}

// Reset the weekly highscores.
func ResetWeekly(db *sql.DB, t time.Time) error {
	query := `UPDATE users SET weekly_wins = $1, weekly_losses = $2`
	if _, err := db.Exec(query, 0, 0); err != nil {
		return err
	}

	timestr := t.Format(time.RFC3339)
	query = `UPDATE meta SET weekly = $1`
	if _, err := db.Exec(query, timestr); err != nil {
		return err
	}

	return nil
}
