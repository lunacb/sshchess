package protocol

import (
	"fmt"
	"strings"

	"codeberg.org/lunacb/sshchess/lib/board"
	"codeberg.org/lunacb/sshchess/lib/consts"
	"codeberg.org/lunacb/sshchess/lib/parse"
)

type Msg interface {
	Log() (msg string, send bool)
}

type MsgTerm struct{}
type MsgRandomMatch struct{}
type MsgWaitingQuit struct{}
type MsgWatchQuit struct{}
type MsgGameQuit struct{}
type MsgGameRematch struct{}
type MsgSendVersion struct{}
type MsgGameMove = board.MovePiece

func (m MsgTerm) Log() (string, bool) { return "term", true }
func (m MsgRandomMatch) Log() (string, bool) { return "random match", true }
func (m MsgWaitingQuit) Log() (string, bool) { return "waiting quit", true }
func (m MsgWatchQuit) Log() (string, bool) { return "watch quit", true }
func (m MsgGameQuit) Log() (string, bool) { return "game quit", true }
func (m MsgGameRematch) Log() (string, bool) { return "game rematch", true }
func (m MsgSendVersion) Log() (string, bool) { return "send version", true }

type MsgInvalid struct {
	msg string
}

func (m MsgInvalid) Log() (string, bool) {
	return m.msg, true
}

type MsgLogin struct {
	Guest bool
	Username string
	Password string
}

func (m MsgLogin) Log() (string, bool) {
	if m.Guest {
	return fmt.Sprintf("guest login: username=%s", m.Username), true
	} else {
		return fmt.Sprintf("user login: username=%s", m.Username), true
	}
}

type MsgCreateAccount struct {
	Username string
	Password string
}

func (m MsgCreateAccount) Log() (string, bool) {
	return fmt.Sprintf("create account: username=%s", m.Username), true
}

type MsgSearchUser struct {
	Search string
	Offline bool
}

func (m MsgSearchUser) Log() (string, bool) {
	return fmt.Sprintf("search: search=%s offline=%t", m.Search, m.Offline), true
}

type MsgChallengeUser struct {
	Name string
	Guest bool
	Accepting bool
}

func (m MsgChallengeUser) Log() (string, bool) {
	return fmt.Sprintf("challenge user: guest=%t name=%s accepting=%t", m.Guest, m.Name, m.Accepting), true
}

type MsgWatchMatch struct {
	NameFirst, NameSecond string
	GuestFirst, GuestSecond bool
}

func (m MsgWatchMatch) Log() (string, bool) {
	return fmt.Sprintf("watch match: first guest=%t name=%s second guest=%t name=%s",
		m.GuestFirst, m.NameFirst, m.GuestSecond, m.NameSecond), true
}

func ParseLine(line string) (Msg, string) {
	argv := strings.Split(line, " ")

	id := argv[0]
	argv = argv[1:]
	if id == "" { id = "NULL" }

	if len(argv) == 0 { return MsgInvalid{line}, id }

	switch argv[0] {
	case consts.ClientLogin:
		// 2+ args
		if len(argv) < 3 { return MsgInvalid{line}, id }

		res := MsgLogin{}
		if argv[1] == "guest" {
			// 2 args
			if len(argv) != 3 { return MsgInvalid{line}, id }

			res.Guest = true
			if !parse.UsernameGood(argv[2]) { return MsgInvalid{line}, id }
			res.Username = argv[2]
			res.Password = ""

		} else if argv[1] == "user" {
			// 3 args
			if len(argv) != 4 { return MsgInvalid{line}, id }

			res.Guest = false
			if !parse.UsernameGood(argv[2]) { return MsgInvalid{line}, id }
			res.Username = argv[2]
			if !parse.PasswordGood(argv[3]) { return MsgInvalid{line}, id }
			res.Password = argv[3]

		} else {
			return MsgInvalid{line}, id
		}

		return res, id

	case consts.ClientCreateAccount:
		// 2 args
		if len(argv) != 3 { return MsgInvalid{line}, id }

		res := MsgCreateAccount{}

		// Username
		if !parse.UsernameGood(argv[1]) { return MsgInvalid{line}, id }
		res.Username = argv[1]
		if !parse.PasswordGood(argv[2]) { return MsgInvalid{line}, id }
		res.Password = argv[2]

		return res, id

	case consts.ClientTerm:
		// 0 args
		if len(argv) != 1 { return MsgInvalid{line}, id }

		return MsgTerm{}, id

	case consts.ClientRandomMatch:
		// 0 args
		if len(argv) != 1 { return MsgInvalid{line}, id }

		return MsgRandomMatch{}, id

	case consts.ClientChallengeUser:
		// 2 args
		if len(argv) != 3 { return MsgInvalid{line}, id }

		var good bool

		res := MsgChallengeUser{}

		res.Accepting = false

		res.Guest, good = parse.GetGuest(argv[1])
		if !good { return MsgInvalid{line}, id }

		res.Name = argv[2]

		return res, id

	case consts.ClientAcceptChallenge:
		// 2 args
		if len(argv) != 3 { return MsgInvalid{line}, id }

		var good bool

		res := MsgChallengeUser{}

		res.Accepting = true

		res.Guest, good = parse.GetGuest(argv[1])
		if !good { return MsgInvalid{line}, id }

		res.Name = argv[2]

		return res, id

	case consts.ClientWatchMatch:
		// 4 args
		if len(argv) != 5 { return MsgInvalid{line}, id }

		var good bool

		res := MsgWatchMatch{}
		res.GuestFirst, good = parse.GetGuest(argv[1])
		if !good { return MsgInvalid{line}, id }

		res.NameFirst = argv[2]

		res.GuestSecond, good = parse.GetGuest(argv[3])
		if !good { return MsgInvalid{line}, id }

		res.NameSecond = argv[4]

		return res, id

	case consts.ClientWatchQuit:

		// 0 args
		if len(argv) != 1 { return MsgInvalid{line}, id }

		return MsgWatchQuit{}, id

	case consts.ClientWaitingQuit:
		// 0 args
		if len(argv) != 1 { return MsgInvalid{line}, id }

		return MsgWaitingQuit{}, id

	case consts.ClientSearchUser:
		var search string
		// 0 or 1 args
		if len(argv) != 3 {
			if len(argv) != 2 { return MsgInvalid{line}, id }
			search = ""
		} else {
			search = argv[2]
		}

		var good bool

		res := MsgSearchUser{}
		res.Search = search

		res.Offline, good = parse.GetBool(argv[1])
		if !good { return MsgInvalid{line}, id }

		return res, id

	case consts.ClientGameMove:
		// 2+ args
		if len(argv) < 3 { return MsgInvalid{line}, id }

		var good bool

		res := MsgGameMove{}
		if res.X, res.Y, good = parse.GetBoardPos(argv[1], argv[2]); !good {
			return MsgInvalid{line}, id
		}

		if res.Piece, good = boardParseArgs(argv[3:]); !good {
			return MsgInvalid{line}, id
		}

		return res, id

	case consts.ClientGameQuit:
		// 0 args
		if len(argv) != 1 { return MsgInvalid{line}, id }

		return MsgGameQuit{}, id

	case consts.ClientGameRematch:
		// 0 args
		if len(argv) != 1 { return MsgInvalid{line}, id }

		return MsgGameRematch{}, id

	case consts.ClientSendVersion:
		// 0 args
		if len(argv) != 1 { return MsgInvalid{line}, id }

		return MsgSendVersion{}, id

	default: return MsgInvalid{line}, id
	}

	return MsgInvalid{line}, id
}
