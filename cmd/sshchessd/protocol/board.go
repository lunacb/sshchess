package protocol

import (
	"strconv"

	"codeberg.org/lunacb/sshchess/lib/board"
	"codeberg.org/lunacb/sshchess/lib/parse"
)

func getPieceDir(str string, straight bool, diag bool) (board.MoveType, bool) {
	if straight {
		switch str {
		case "up": return board.MoveUp, true
		case "down": return board.MoveDown, true
		case "left": return board.MoveLeft, true
		case "right": return board.MoveRight, true
		default:
		}
	}
	if diag {
		switch str {
		case "upleft": return board.MoveUp | board.MoveLeft, true
		case "downleft": return board.MoveDown | board.MoveLeft, true
		case "upright": return board.MoveUp | board.MoveRight, true
		case "downright": return board.MoveDown | board.MoveRight, true
		default: 
		}
	}

	return 0, false
}

func getPawnDir(str string) (board.MoveType, bool) {
	switch str {
	case "left": return board.MoveLeft, true
	case "right": return board.MoveRight, true
	case "forwards": return board.MoveForwards, true
	case "none": return 0, true
	default:
	}

	return 0, false
}

func getCastleDir(str string) (board.MoveType, bool) {
	switch str {
	case "left": return board.MoveLeft, true
	case "right": return board.MoveRight, true
	default:
	}

	return 0, false
}

func getKnightDir(two string, one string) (board.MoveKnight, bool) {
	res := board.MoveKnight{}
	var good bool
	if res.Two, good = getPieceDir(two, true, false); !good {
		return board.MoveKnight{}, false
	}
	if res.One, good = getPieceDir(one, true, false); !good {
		return board.MoveKnight{}, false
	}
	if res.Two == board.MoveUp || res.Two == board.MoveDown {
		if res.One != board.MoveLeft && res.One != board.MoveRight { return board.MoveKnight{}, false }
	} else {
		if res.One != board.MoveUp && res.One != board.MoveDown { return board.MoveKnight{}, false }
	}

	return res, true
}

func boardParseArgs(argv []string) (interface{}, bool) {
	// 1+ args
	if len(argv) < 1 { return nil, false }

	switch(argv[0]) {
	case "king":
		// 1 args
		if len(argv) != 2 { return nil, false }

		var good bool

		res := board.MoveKing{}
		res.Move, good = getPieceDir(argv[1], true, true)
		if !good { return nil, false }

		return res, true

	case "queen":
		// 2 args
		if len(argv) != 3 { return nil, false }

		var good bool
		var err error

		res := board.MoveQueen{}
		res.Move, good = getPieceDir(argv[1], true, true)
		if !good { return nil, false }

		res.Length, err = strconv.Atoi(argv[2])
		if err != nil  { return nil, false }

		return res, true

	case "rook":
		// 2 args
		if len(argv) != 3 { return nil, false }

		var good bool
		var err error

		res := board.MoveRook{}
		res.Move, good = getPieceDir(argv[1], true, false)
		if !good { return nil, false }

		res.Length, err = strconv.Atoi(argv[2])
		if err != nil  { return nil, false }

		return res, true

	case "bishop":
		// 2 args
		if len(argv) != 3 { return nil, false }

		var good bool
		var err error

		res := board.MoveBishop{}
		res.Move, good = getPieceDir(argv[1], false, true)
		if !good { return nil, false }

		res.Length, err = strconv.Atoi(argv[2])
		if err != nil  { return nil, false }

		return res, true

	case "knight":
		// 2 args
		if len(argv) != 3 { return nil, false }

		res, good := getKnightDir(argv[1], argv[2])
		if !good { return nil, false }

		return res, true

	case "pawn":
		// 2 args
		if len(argv) != 3 { return nil, false }

		var good bool

		res := board.MovePawn{}
		res.Move, good = getPawnDir(argv[1])
		if !good { return nil, false }

		res.Promote, good = parse.GetPromote(argv[2])
		if !good { return nil, false }

		return res, true

	case "castle":
		// 1 args
		if len(argv) != 2 { return nil, false }

		var good bool

		res := board.MoveCastle{}
		res.Move, good = getCastleDir(argv[1])
		if !good { return nil, false }

		return res, true

	default: return nil, false
	}
}
