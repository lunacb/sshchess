package screen

import (
	"github.com/gdamore/tcell/v2"

	"codeberg.org/lunacb/sshchess/lib/consts"
)

type Styles struct {
	Normal, Help, HelpBold, Input, Column, Error tcell.Style
	WhiteKing, BlackKing, White, Black tcell.Style
}

// An object on the screen.
type Item struct {
	Start int // The starting y value for the item.
	End int // The ending y value for the item.
}

type ItemSpec struct {
	Ptr **Item // The *Item pointed to will be set to a pointer to a generated Item.
	Length int // Vertical length of the item
	Selectable bool
}

type Screen struct {
	Tc tcell.Screen
	Styles Styles
	CursorVisible bool
	CursorX, CursorY int
}

type Menu struct {
	S *Screen
	Items []Item
	Pos int // Focused item index.
	// Secifies an index in Items, or -1 if none.
	QueuedFocus, QueuedSelect int
}

type RuneRange func (r rune) bool

func GraphRunes(r rune) bool {
	return (r >= consts.NameCharStart && r <= consts.NameCharEnd)
}
func ChessRunes(r rune) bool {
	return ((r >= '1' && r <= '8') || (r >= 'a' && r <= 'h'))
}

type Textbox struct {
	S *Screen
	I *Item
	X int
	Text string
	Pass bool
	MaxLen int
	// List of positions each character entered into the text box should be placed at, in order.
	PosMap []int
	Runes RuneRange
}

const PosStart int = 2

func (s *Screen) ShowCursor(x int, y int) {
	s.CursorVisible = true
	s.CursorX = x
	s.CursorY = y
	s.Tc.ShowCursor(x, y)
}

func (s *Screen) HideCursor() {
	s.CursorVisible = false
	s.Tc.HideCursor()
}

func (s *Screen) DrawText(x, y int, style tcell.Style, text string) {
	for i, r := range []rune(text) {
		s.Tc.SetContent(x + i, y, r, nil, style)
	}
}

func (s *Screen) DrawTextReverse(x, y int, style tcell.Style, text string) {
	runes := []rune(text)
	for i, r := range runes {
		s.Tc.SetContent(x + (len(runes)-1-i), y, r, nil, style)
	}
}

// Draw a keybinding help message. In "KEY: ACTION", "KEY" will be hilighted bold.
func (s *Screen) DrawHelp(x, y int, norm tcell.Style, bold tcell.Style, text string) {
	style := norm
	runes := []rune(text)
	for i := len(runes) - 1; i >= 0; i-- {
		r := runes[i]
		if r == ':' {
			style = bold
		} else if r == ' ' || r == '(' {
			style = norm
		}
		s.Tc.SetContent(x + i, y, r, nil, style)
	}
}

// Generate a list of items from ItemSpec's
func MakeItems(spec []ItemSpec) []Item {
	items := make([]Item, 0, len(spec))
	pos := PosStart
	for _, v := range spec {
		item := Item{
			Start: pos,
			End: pos + v.Length,
		}
		if v.Selectable {
			items = append(items, item)
			*v.Ptr = &items[len(items)-1]
		} else {
			heap := new(Item)
			*heap = item
			*v.Ptr = heap
		}
		pos += v.Length
	}

	return items
}

func NewMenu(s *Screen, items []Item) Menu {
	return Menu {
		S: s,
		Items: items,
		Pos: 0,
		QueuedFocus: -1,
		QueuedSelect: -1,
	}
}

func NewTextbox(m *Menu, i *Item, x int, pass bool, runes RuneRange, maxLen int) Textbox {
	return Textbox {
		S: m.S,
		I: i,
		X: x,
		Text: "",
		Pass: pass,
		MaxLen: maxLen,
		PosMap: []int{},
		Runes: runes,
	}
}

func NewTextboxMap(m *Menu, i *Item, x int, pass bool, runes RuneRange, posMap []int) Textbox {
	return Textbox {
		S: m.S,
		I: i,
		X: x,
		Text: "",
		Pass: pass,
		MaxLen: len(posMap) - 1,
		PosMap: posMap,
		Runes: runes,
	}
}

func (m *Menu) QueueFocus(item *Item) {
	for i, _ := range m.Items {
		if &m.Items[i] == item {
			m.QueuedFocus = i
			return
		}
	}

	panic("unknown item")
}

func (m *Menu) QueueSelect(item *Item) {
	for i, _ := range m.Items {
		if &m.Items[i] == item {
			m.QueuedSelect = i
			return
		}
	}

	panic("unknown item")
}

// Draw item numbers and an indicator for the focused item.
func (m *Menu) Draw() {
	if len(m.Items) == 0 {
		return
	}

	if m.Pos >= len(m.Items) {
		m.Pos = len(m.Items) - 1
	}

	for i, v := range m.Items {
		var numRune rune
		if i == 9 {
			numRune = '0'
		} else {
			numRune = '1' + rune(i)
		}
		m.S.Tc.SetContent(2, v.Start, numRune, nil, m.S.Styles.Normal)
		m.S.Tc.SetContent(3, v.Start, ')', nil, m.S.Styles.Normal)
	}
	m.S.Tc.SetContent(0, m.Items[m.Pos].Start, '>', nil, m.S.Styles.Normal)
	m.S.Tc.Show()
}

func (m *Menu) SetArrowVisible(visible bool) {
	if len(m.Items) == 0 {
		return
	}

	var r rune
	if visible {
		r = '>'
	} else {
		r = ' '
	}
	m.S.Tc.SetContent(0, m.Items[m.Pos].Start, r, nil, m.S.Styles.Normal)
}

func (t Textbox) Draw() {
	for i, v := range []rune(t.Text) {
		r := v
		if t.Pass { r = '*' }
		t.S.Tc.SetContent(t.GetX(i), t.I.Start, r, nil, t.S.Styles.Input)
	}
}

func (t Textbox) ShowCursor() {
	t.S.ShowCursor(t.X + len(t.Text), t.I.Start)
	t.S.Tc.Show()
}

func (t Textbox) HideCursor() {
	t.S.HideCursor()
	t.S.Tc.Show()
}

func  (t *Textbox) GetX(index int) int {
	if len(t.PosMap) != 0 {
		return t.X + t.PosMap[index]
	} else {
		return t.X + index
	}
}

func Init() (Screen, error) {
	resetBase := tcell.StyleDefault.Background(tcell.ColorReset)
	styles := Styles{
		Normal: resetBase.Foreground(tcell.ColorReset),
		Help: resetBase.Foreground(tcell.ColorPurple),
		HelpBold: resetBase.Foreground(tcell.ColorPurple).Bold(true),
		Input: resetBase.Foreground(tcell.ColorGreen),
		Column: resetBase.Foreground(tcell.ColorGray),
		Error: resetBase.Foreground(tcell.ColorRed),
		WhiteKing: resetBase.Foreground(tcell.ColorNavy).Bold(true),
		BlackKing: resetBase.Foreground(tcell.ColorRed).Bold(true),
		White: resetBase.Foreground(tcell.ColorWhite).Bold(true),
		Black: resetBase.Foreground(tcell.ColorGray).Bold(true),
	}

	var s tcell.Screen
	var err error

	if s, err = tcell.NewScreen(); err != nil {
		return Screen{}, err
	}

	if err = s.Init(); err != nil {
		return Screen{}, err
	}

	s.SetStyle(styles.Normal)
	s.Clear()

	return Screen{
		Tc: s,
		Styles: styles,
		CursorVisible: false,
		CursorX: 0,
		CursorY: 0,
	}, nil
}
