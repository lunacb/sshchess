package screen

import (
	"github.com/gdamore/tcell/v2"

	"codeberg.org/lunacb/sshchess/cmd/sshchess/protocol"
	"codeberg.org/lunacb/sshchess/lib/board"
	"codeberg.org/lunacb/sshchess/lib/parse"
)

// TODO: make this a method of Screen
func (s *Screen) DrawBoard(b board.Board, team board.GameSide, x int, y int) {
	boardStr := []string{
		"  +---+---+---+---+---+---+---+---+",
		"8 |   |   |   |   |   |   |   |   |",
		"  +---+---+---+---+---+---+---+---+",
		"7 |   |   |   |   |   |   |   |   |",
		"  +---+---+---+---+---+---+---+---+",
		"6 |   |   |   |   |   |   |   |   |",
		"  +---+---+---+---+---+---+---+---+",
		"5 |   |   |   |   |   |   |   |   |",
		"  +---+---+---+---+---+---+---+---+",
		"4 |   |   |   |   |   |   |   |   |",
		"  +---+---+---+---+---+---+---+---+",
		"3 |   |   |   |   |   |   |   |   |",
		"  +---+---+---+---+---+---+---+---+",
		"2 |   |   |   |   |   |   |   |   |",
		"  +---+---+---+---+---+---+---+---+",
		"1 |   |   |   |   |   |   |   |   |",
		"  +---+---+---+---+---+---+---+---+",
	}
	letterStr := "a   b   c   d   e   f   g   h"

	// Draw the board, adjusting it's orientation based on the team.
	if team == board.TWhite {
		for i, v := range boardStr {
			s.DrawText(x, y+i, s.Styles.Normal, v)
		}
		s.DrawText(x+4, y+len(boardStr), s.Styles.Normal, letterStr)
	} else {
		for i, v := range boardStr {
			s.DrawText(x, y+(len(boardStr)-1-i), s.Styles.Normal, v)
		}
		s.DrawTextReverse(x+4, y+len(boardStr), s.Styles.Normal, letterStr)
	}

	for i, row := range b.Board {
		for j, v := range row {
			var pieceRune, teamRune rune = 0, 0

			if v.Side == board.TWhite {
				teamRune = 'w'
			} else {
				teamRune = 'b'
			}

			switch v.Piece {
			case board.PKing: pieceRune = 'K'
			case board.PQueen: pieceRune = 'Q'
			case board.PRook: pieceRune = 'r'
			case board.PBishop: pieceRune = 'b'
			case board.PKnight: pieceRune = 'k'
			case board.PPawn: pieceRune = 'p'
			}

			if pieceRune != 0 {
				// Put the piece on the board.

				str := string(teamRune) + string(pieceRune)
				var color tcell.Style
				if v.Side == board.TWhite {
					if v.Piece == board.PKing {
						color = s.Styles.WhiteKing
					} else {
						color = s.Styles.White
					}
				} else {
					if v.Piece == board.PKing {
						color = s.Styles.BlackKing
					} else {
						color = s.Styles.Black
					}
				}

				// Draw the piece, adjusting orientation appropriately based on
				// team.
				if team == board.TWhite {
					s.DrawText(x + 3 + j*4, y + 1 + i*2, color, str)
				} else {
					s.DrawText(x + 3 + (len(row)-1-j)*4,
						y + 1 + (len(b.Board)-1-i)*2, color, str)
				}
			}
		}
	}
}

// Draws a status indicator showing who's playing and who's turn it is.
func (s *Screen) DrawTurns(first, second protocol.User, isFirst bool, firstSide board.GameSide, y int) {
	sfirst := parse.DisplayUser(first.Name, first.Guest)
	ssecond := parse.DisplayUser(second.Name, second.Guest)
	if firstSide == board.TWhite {
		sfirst = "[white] " + sfirst
		ssecond = "[black] " + ssecond
	} else {
		ssecond = "[white] " + ssecond
		sfirst = "[black] " + sfirst
	}

	if isFirst {
		sfirst = ">> " + sfirst
	} else {
		ssecond = ">> " + ssecond
	}

	s.DrawText(5, y, s.Styles.Normal, sfirst)
	s.DrawText(5, y + 1, s.Styles.Normal, ssecond)
}
