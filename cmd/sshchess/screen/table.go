package screen

import (
	"github.com/gdamore/tcell/v2"
	"strconv"

	"codeberg.org/lunacb/sshchess/cmd/sshchess/protocol"
)

// Space between columns.
const TableMargin int = 3

// Draw a two-dimensional matrix of strings. each of `members` is a heading for
// a column, and if the number of items in a row exceeds the number of members
// those excess members won't be drawn.
func (s *Screen) DrawTable(x int, y int, headStyle tcell.Style,
		style tcell.Style, table [][]string, members ...string) {
	offsets := make([]int, len(members))
	offset := x

	table = append([][]string{members}, table...)

	for i, _ := range offsets {
		offsets[i] = offset
		length := 0
		for _, v := range table {
			if i >= len(v) { continue }
			if len(v[i]) > length { length = len(v[i]) }
		}
		offset += length + TableMargin
	}

	for i, v := range table[0] {
		if i >= len(offsets) { break }
		s.DrawText(offsets[i], y, headStyle, v)
	}
	for i, v := range table[1:] {
		for j, v := range v {
			if j >= len(offsets) { break }
			s.DrawText(offsets[j] + 1, i + y + 1, style, v)
		}
	}
}

func TableFromUsers(table [][]protocol.User) [][]string {
	newTable := make([][]string, len(table))
	for i, v := range table {
		newTable[i] = []string{}
		for _, v := range v {
			if v.Guest {
				newTable[i] = append(newTable[i], v.Name + " (guest)")
			} else {
				newTable[i] = append(newTable[i], v.Name)
			}
		}
	}
	return newTable
}

func TableFromUserStats(table [][]protocol.UserStat) [][]string {
	newTable := make([][]string, len(table))
	for i, v := range table {
		newTable[i] = []string{}
		for _, v := range v {
			if v.Guest {
				newTable[i] = append(newTable[i], v.Name + " (guest)", "0", "0")
			} else {
				newTable[i] = append(newTable[i], v.Name, strconv.Itoa(v.Wins), strconv.Itoa(v.Losses))
			}
		}
	}
	return newTable
}

func TableFromSearch(table []protocol.SearchItem) [][]string {
	newTable := make([][]string, len(table))
	for i, v := range table {
		u := v.Stat
		var online string
		if v.Online {
			online = "online"
		} else {
			online = "offline"
		}
		if u.Guest {
			newTable[i] = []string{u.Name + " (guest)", online, "0", "0"}
		} else {
			newTable[i] = []string{u.Name, online, strconv.Itoa(u.Wins), strconv.Itoa(u.Losses)}
		}
	}
	return newTable
}

// Returns an adjusted table. If the number of rows in `table` is less than
// `size`, then rows containing `rowsize` columns with "--" as the contents of
// each item will be appended to fill the gap. If the number of rows is
// greater than `size`, then a new row will be added only containing "[...]".
func TableWithLength(table [][]string, rowsize int, size int) [][]string {
	blankrow := make([]string, rowsize)
	for i := 0; i < rowsize; i += 1 {
		blankrow[i] = "--"
	}
	for len(table) < size {
		table = append(table, blankrow)
	}

	if len(table) > size {
		table = table[:size]
		table = append(table, []string{"[...]"})
	}

	return table
}
