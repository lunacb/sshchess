package main

import (
	"fmt"
	"github.com/pborman/getopt/v2"
	"net"
	"os"

	"codeberg.org/lunacb/sshchess/cmd/sshchess/exit"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/gamestate"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/menus"
)

func main() {
	usage := false
	sockpath := "/tmp/sshchess.sock"

	getopt.FlagLong(&usage, "help", 'h', "Print this message")
	getopt.FlagLong(&sockpath, "socket", 's', "Path to the server socket")
	getopt.Parse()

	if usage {
		getopt.Usage()
		os.Exit(0)
	}

	conn, err := net.Dial("unix", sockpath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error connecting to %s: %s\n", sockpath, err)
		os.Exit(1)
	}

	state, good := gamestate.NewState(conn)
	if !good { os.Exit(1) }

	user, good := menus.LoginMenu(&state)
	state.LoggedIn = true
	state.User = user
	if good {
		menus.HomeMenu(&state)
	}

	exit.Exit(0, &state)
}
