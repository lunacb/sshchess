package protocol

import (
	"codeberg.org/lunacb/sshchess/lib/board"
	"codeberg.org/lunacb/sshchess/lib/consts"
	"codeberg.org/lunacb/sshchess/lib/parse"

	"strconv"
	"strings"
)

type User struct {
	Name string
	Guest bool
}

type UserStat struct {
	Name string
	Guest bool
	Wins, Losses int
}

type Msg interface{}

type MsgOk struct {
	Id int
}

type MsgErr struct {
	Id int
	Err string
}

type Move struct {
	X1, Y1, X2, Y2 int
	Promote board.PieceName
}

type MsgInvalid struct{
	Msg string
}

type MsgTerm struct{}
type MsgWaitingQuit struct{}
type MsgGameAlone struct{}
type MsgGameTurn struct{}
type MsgGameWantRematch struct{}
type MsgWatchDestroy struct{}

type MsgVersion struct{
	Major, Minor, Patch int
}

type MsgWatchBegin struct {
	White, Black User
	Turn board.GameSide
	Board board.Board
}

type MsgWatchEnd struct {
	Team board.GameSide
}

type MsgWatchTurn struct {
	Team board.GameSide
}

type MsgWatchMove struct {
	Moves []Move
}

// TODO: watch.*

type MsgAddChallenge struct {
	Who User
}

type MsgRmChallenge struct {
	Who User
}

type MsgAddMatch struct {
	One, Two User
}

type MsgRmMatch struct {
	One, Two User
}

type SearchItem struct {
	Stat UserStat
	Online bool
}

type MsgSearch struct {
	Search []SearchItem
	Truncated bool
}

type MsgLeaderboard struct {
	Alltime bool
	Leaderboard []UserStat
}

type MsgGameBegin struct {
	Opponent User
	Team board.GameSide
}

type MsgGameMove struct {
	Moves []Move
}

type MsgGameEnd struct {
	Team board.GameSide
}

func getUser(argv []string) (res User, resGood bool) {
	var good bool

	res = User{}

	res.Guest, good = parse.GetGuest(argv[0])
	if !good { return User{}, false }

	res.Name = argv[1]

	return res, true
}

func getUserStat(argv []string) (res UserStat, resGood bool) {
	var good bool
	var err error

	res = UserStat{}

	res.Guest, good = parse.GetGuest(argv[0])
	if !good { return UserStat{}, false }

	res.Name = argv[1]

	res.Wins, err = strconv.Atoi(argv[2])
	if err != nil { return UserStat{}, false }

	res.Losses, err = strconv.Atoi(argv[3])
	if err != nil { return UserStat{}, false }

	return res, true
}

func ParseLine(line string) (Msg) {
	argv := strings.Split(line, " ")

	if len(argv) == 0 { return MsgInvalid{ Msg: line } }

	switch argv[0] {
	case consts.ServerOk:
		// 1 arg
		if len(argv) != 2 { return MsgInvalid{ Msg: line } }

		var err error

		res := MsgOk{}
		if res.Id, err = strconv.Atoi(argv[1]); err != nil {
			return MsgInvalid{ Msg: line }
		}

		return res

	case consts.ServerErr:
		// 2 args
		if len(argv) != 3 { return MsgInvalid{ Msg: line } }

		var err error

		res := MsgErr{}
		// TODO: sanitize this
		res.Err = argv[1]

		if res.Id, err = strconv.Atoi(argv[2]); err != nil {
			return MsgInvalid{ Msg: line }
		}

		return res

	case consts.ServerTerm:
		// 0 args
		if len(argv) != 1 { return MsgInvalid{ Msg: line } }

		return MsgTerm{}

	case consts.ServerAddChallenge:
		// 2 args
		if len(argv) != 3 { return MsgInvalid{ Msg: line } }

		var good bool

		res := MsgAddChallenge{}
		res.Who, good = getUser(argv[1:3])
		if !good { return MsgInvalid{ Msg: line } }

		return res

	case consts.ServerRmChallenge:
		// 2 args
		if len(argv) != 3 { return MsgInvalid{ Msg: line } }

		var good bool

		res := MsgRmChallenge{}
		res.Who, good = getUser(argv[1:3])
		if !good { return MsgInvalid{ Msg: line } }

		return res

	case consts.ServerAddMatch:
		// 4 args
		if len(argv) != 5 { return MsgInvalid{ Msg: line } }

		var good bool

		res := MsgAddMatch{}
		res.One, good = getUser(argv[1:3])
		if !good { return MsgInvalid{ Msg: line } }

		res.Two, good = getUser(argv[3:5])
		if !good { return MsgInvalid{ Msg: line } }

		return res

	case consts.ServerRmMatch:
		// 4 args
		if len(argv) != 5 { return MsgInvalid{ Msg: line } }

		var good bool

		res := MsgRmMatch{}
		res.One, good = getUser(argv[1:3])
		if !good { return MsgInvalid{ Msg: line } }

		res.Two, good = getUser(argv[3:5])
		if !good { return MsgInvalid{ Msg: line } }

		return res

	case consts.ServerSearch:

		res := MsgSearch{}
		res.Search = []SearchItem{}
		res.Truncated = false

		for argv = argv[1:]; len(argv) > 0; argv = argv[5:] {
			var good bool

			// 5 args per user
			if len(argv) < 5 {
				if len(argv) == 1 && argv[0] == "trunc" {
					res.Truncated = true
				} else {
					return MsgInvalid{ Msg: line }
				}
			}

			var newuser SearchItem
			newuser.Stat, good = getUserStat(argv[1:5])
			if !good { return MsgInvalid{ Msg: line } }

			if argv[0] == "online" {
				newuser.Online = true
			} else if argv[0] == "offline" {
				newuser.Online = false
			} else {
				return MsgInvalid{ Msg: line }
			}

			res.Search = append(res.Search, newuser)
		}

		return res

	case consts.ServerLeaderboard:
		// 1+ args
		if len(argv) < 2 { return MsgInvalid{ Msg: line } }

		var good bool

		res := MsgLeaderboard{}
		res.Alltime, good = parse.GetAlltime(argv[1])
		if !good { return MsgInvalid{ Msg: line } }

		users := []UserStat{}

		for argv = argv[2:]; len(argv) > 0; argv = argv[4:] {
			var good bool

			// 4 args per user
			if len(argv) < 4 { return MsgInvalid{ Msg: line } }

			var newuser UserStat
			newuser, good = getUserStat(argv[0:4])
			if !good { return MsgInvalid{ Msg: line } }

			users = append(users, newuser)
		}

		res.Leaderboard = users
		return res

	case consts.ServerGameBegin:
		// 3 args
		if len(argv) != 4 { return MsgInvalid{ Msg: line } }

		var good bool

		// TODO: make a function for parsing userstats
		res := MsgGameBegin{}
		res.Team, good = parse.GetColor(argv[1])
		if !good { return MsgInvalid{ Msg: line } }

		res.Opponent, good = getUser(argv[2:4])
		if !good { return MsgInvalid{ Msg: line } }

		return res

	case consts.ServerGameAlone:
		// 0 args
		if len(argv) != 1 { return MsgInvalid{ Msg: line } }

		return MsgGameAlone{}

	case consts.ServerGameTurn:
		// 0 args
		if len(argv) != 1 { return MsgInvalid{ Msg: line } }

		return MsgGameTurn{}

	case consts.ServerGameEnd:
		// 1 arg
		if len(argv) != 2 { return MsgInvalid{ Msg: line } }

		var good bool

		res := MsgGameEnd{}
		res.Team, good = parse.GetColor(argv[1])
		if !good { return MsgInvalid{ Msg: line } }

		return res

	case consts.ServerGameWantRematch:
		// 0 args
		if len(argv) != 1 { return MsgInvalid{ Msg: line } }

		return MsgGameWantRematch{}

	case consts.ServerWaitingQuit:
		// 0 args
		if len(argv) != 1 { return MsgInvalid{ Msg: line } }

		return MsgWaitingQuit{}

	case consts.ServerWatchDestroy:
		// 0 args
		if len(argv) != 1 { return MsgInvalid{ Msg: line } }

		return MsgWatchDestroy{}

	case consts.ServerWatchBegin:
		// 6+ args
		if len(argv) < 7 { return MsgInvalid{ Msg: line } }

		var good bool
		
		var first, second User
		var firstColor, secondColor board.GameSide

		res := MsgWatchBegin{}
		first, good = getUser(argv[1:3])
		if !good { return MsgInvalid{ Msg: line } }
		second, good = getUser(argv[4:6])
		if !good { return MsgInvalid{ Msg: line } }

		firstColor, good = parse.GetColor(argv[3])
		if !good { return MsgInvalid{ Msg: line } }
		secondColor, good = parse.GetColor(argv[6])
		if !good { return MsgInvalid{ Msg: line } }

		if firstColor == secondColor { return MsgInvalid{ Msg: line } }

		if firstColor == board.TWhite {
			res.White = first
			res.Black = second
		} else {
			res.White = second
			res.Black = first
		}
		res.Turn = firstColor

		res.Board = board.EmptyBoard()
		for argv = argv[7:]; len(argv) > 0; argv = argv[4:] {
			var good bool

			// 4 args per move
			if len(argv) < 4 { return MsgInvalid{ Msg: "oh" } }

			piece := board.Piece{}
			piece.Moved = false
			var x, y int

			x, y, good = parse.GetBoardPos(argv[0], argv[1])

			piece.Piece, good = parse.GetPiece(argv[2])
			if !good { return MsgInvalid{ Msg: line } }

			piece.Side, good = parse.GetColor(argv[3])
			if !good { return MsgInvalid{ Msg: line } }

			res.Board.Board[x][y] = piece
		}

		return res

	case consts.ServerWatchEnd:
		// 1 arg
		if len(argv) != 2 { return MsgInvalid{ Msg: line } }

		var good bool

		res := MsgWatchEnd{}
		res.Team, good = parse.GetColor(argv[1])
		if !good { return MsgInvalid{ Msg: line } }

		return res

	case consts.ServerWatchTurn:
		// 1 arg
		if len(argv) != 2 { return MsgInvalid{ Msg: line } }

		var good bool

		res := MsgWatchTurn{}
		res.Team, good = parse.GetColor(argv[1])
		if !good { return MsgInvalid{ Msg: line } }

		return res

	case consts.ServerWatchMove:
		// 5+ args
		if len(argv) < 6 { return MsgInvalid{ Msg: line } }

		res := MsgWatchMove{}
		moves := []Move{}

		for argv = argv[1:]; len(argv) > 0; argv = argv[5:] {
			var good bool

			// 5 args per move
			if len(argv) < 5 { return MsgInvalid{ Msg: line } }

			var newmove Move

			newmove.X1, newmove.Y1, good = parse.GetBoardPos(argv[0], argv[1])
			if !good { return MsgInvalid{ Msg: line } }

			newmove.X2, newmove.Y2, good = parse.GetBoardPos(argv[2], argv[3])
			if !good { return MsgInvalid{ Msg: line } }

			newmove.Promote, good = parse.GetPiece(argv[4])
			if !good { return MsgInvalid{ Msg: line } }

			moves = append(moves, newmove)
		}

		res.Moves = moves
		return res

	case consts.ServerGameMove:
		// 5+ args
		if len(argv) < 6 { return MsgInvalid{ Msg: line } }

		res := MsgGameMove{}
		moves := []Move{}

		for argv = argv[1:]; len(argv) > 0; argv = argv[5:] {
			var good bool

			// 5 args per move
			if len(argv) < 5 { return MsgInvalid{ Msg: line } }

			var newmove Move

			newmove.X1, newmove.Y1, good = parse.GetBoardPos(argv[0], argv[1])
			if !good { return MsgInvalid{ Msg: line } }

			newmove.X2, newmove.Y2, good = parse.GetBoardPos(argv[2], argv[3])
			if !good { return MsgInvalid{ Msg: line } }

			newmove.Promote, good = parse.GetPiece(argv[4])
			if !good { return MsgInvalid{ Msg: line } }

			moves = append(moves, newmove)
		}

		res.Moves = moves
		return res

	case consts.ServerVersion:
		// 3 args
		if len(argv) != 4 { return MsgInvalid{ Msg: line } }

		var err error

		res := MsgVersion{}
		if res.Major, err = strconv.Atoi(argv[1]); err != nil {
			return MsgInvalid{ Msg: line }
		}

		if res.Minor, err = strconv.Atoi(argv[2]); err != nil {
			return MsgInvalid{ Msg: line }
		}

		if res.Patch, err = strconv.Atoi(argv[3]); err != nil {
			return MsgInvalid{ Msg: line }
		}

		return res
	}

	return MsgInvalid{ Msg: line }
}
