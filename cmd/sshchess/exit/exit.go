package exit

import (
	"os"

	"codeberg.org/lunacb/sshchess/cmd/sshchess/gamestate"
	"codeberg.org/lunacb/sshchess/lib/consts"
)

func Exit(code int, state *gamestate.State) {
	state.Screen.Tc.Fini()
	state.SendMsg(consts.ClientTerm)
	state.Conn.Close()
	os.Exit(code)
}
