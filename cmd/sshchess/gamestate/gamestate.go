package gamestate

import (
	"bufio"
	"fmt"
	"github.com/gdamore/tcell/v2"
	"io"
	"net"
	"os"
	"strconv"
	"strings"

	"codeberg.org/lunacb/sshchess/lib/consts"
	"codeberg.org/lunacb/sshchess/lib/parse"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/protocol"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/screen"
)

type Match struct {
	One, Two protocol.User
}

type State struct {
	TcCh chan tcell.Event
	SockCh chan protocol.Msg
	ErrCh chan error
	Conn net.Conn
	Screen screen.Screen

	LoggedIn bool
	User protocol.User

	KeyGrab func(msg protocol.Msg)

	Challenges []protocol.User
	Alltime []protocol.UserStat
	Weekly []protocol.UserStat
	Matches []Match
}

var msgId int = 0

func NewState(conn net.Conn) (State, bool) {
	state := State{}
	state.Conn = conn
	state.SockCh = make(chan protocol.Msg)
	state.ErrCh = make(chan error)
	state.TcCh = make(chan tcell.Event)
	state.LoggedIn = false

	state.KeyGrab = nil
	state.Challenges = []protocol.User{}
	state.Alltime = []protocol.UserStat{}
	state.Weekly = []protocol.UserStat{}
	state.Matches = []Match{}

	go func() {
		reader := bufio.NewReader(conn)
		for {
			line, err := reader.ReadString('\n')
			if  err != nil {
				state.ErrCh <-err
				return
			}

			if line[len(line)-1] == '\n' { line = line[:len(line)-1] }

			state.SockCh <-protocol.ParseLine(line)
		}
	}()

	id := state.SendMsg(consts.ClientSendVersion)
	for {
		msg := <-state.SockCh
		errstr, good := CheckResponse(msg, id)
		if !good { continue }
		if errstr != "" {
			fmt.Fprintf(os.Stderr, "Failed to get version number\n")
			return State{}, false
		}
		break
	}

	for {
		msg := <-state.SockCh
		good := false
		switch msg := msg.(type) {
		case protocol.MsgVersion:
			if msg.Major != consts.Major || msg.Minor != consts.Minor {
				fmt.Fprintf(os.Stderr, "Incompatible server version\n")
				return State{}, false
			}
			good = true
		}
		if good { break }
	}

	var err error
	if state.Screen, err = screen.Init(); err != nil {
		fmt.Fprintf(os.Stderr, "Failed to initiate screen: %s\n", err)
		return State{}, false
	}

	go func() {
		for {
			state.TcCh <-state.Screen.Tc.PollEvent()
		}
	}()

	return state, true
}

func (s *State) DrawHeader() {
	if s.LoggedIn {
		name := parse.DisplayUser(s.User.Name, s.User.Guest)
		s.Screen.DrawText(0, 0, s.Screen.Styles.Normal, "=== " + name + " === SSHCHESS ===")
	} else {
		s.Screen.DrawText(0, 0, s.Screen.Styles.Normal, "=== SSHCHESS ===")
	}
}

func (s *State) SendMsg(argv ...string) int {
	id := msgId
	msgId += 1
	argv = append([]string{strconv.Itoa(id)}, argv...)
	io.WriteString(s.Conn, strings.Join(argv, " ") + "\n")
	return id
}

// Wait will be called for each message sent through ApplyMsg. Suppresses any
// keypresses by the user from poll functions.
func (s *State) Wait(each func(msg protocol.Msg)) {
	s.KeyGrab = each
}

func (s *State) Unwait() {
	s.KeyGrab = nil
}

func CheckResponse(msg protocol.Msg, id int) (string, bool) {
	switch msg := msg.(type) {
	case protocol.MsgOk:
		if msg.Id == id {
			return "", true
		}

	case protocol.MsgErr:
		if msg.Id == id {
			return msg.Err, true
		}
	}

	return "", false
}

func (s *State) ApplyMsg(ev protocol.Msg) bool {
	switch msg := ev.(type) {
	case protocol.MsgAddMatch:
		s.Matches = append(s.Matches, Match{ msg.One, msg.Two })
		return true

	case protocol.MsgRmMatch:
		for i, v := range s.Matches {
			if v.One.Name == msg.One.Name && v.One.Guest == msg.One.Guest &&
					v.Two.Name == msg.Two.Name && v.Two.Guest == msg.Two.Guest {
				for i := i + 1; i < len(s.Matches); i += 1 {
					s.Matches[i-1] = s.Matches[i]
				}
				s.Matches = s.Matches[:len(s.Matches)-1]
				break
			}
		}
		return true

	case protocol.MsgAddChallenge:
		s.Challenges = append(s.Challenges, msg.Who)
		return true

	case protocol.MsgRmChallenge:
		for i, v := range s.Challenges {
			if v.Name == msg.Who.Name && v.Guest == msg.Who.Guest {
				for i := i + 1; i < len(s.Challenges); i += 1 {
					s.Challenges[i-1] = s.Challenges[i]
				}
				s.Challenges = s.Challenges[:len(s.Challenges)-1]
				break
			}
		}
		return true

	case protocol.MsgLeaderboard:
		if msg.Alltime {
			s.Alltime = msg.Leaderboard
		} else {
			s.Weekly = msg.Leaderboard
		}
		return true
	}

	if s.KeyGrab != nil {
		s.KeyGrab(ev)
		return true
	}

	return false
}
