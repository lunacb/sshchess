package menus

import  (
	"github.com/gdamore/tcell/v2"

	"codeberg.org/lunacb/sshchess/cmd/sshchess/gamestate"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/protocol"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/screen"
	"codeberg.org/lunacb/sshchess/lib/board"
	"codeberg.org/lunacb/sshchess/lib/consts"
	"codeberg.org/lunacb/sshchess/lib/parse"
)

func WatchMenu(state *gamestate.State, match protocol.MsgWatchBegin) {
	var iHelp1, iHelp2, iBoard, iTurn, iEnd *screen.Item

	matchMenu := screen.NewMenu(&state.Screen, screen.MakeItems([]screen.ItemSpec{
		{ &iHelp1, 2, false },
		{ &iBoard, 19, false },
		{ &iTurn, 3, false },
	}))

	endMenu := screen.NewMenu(&state.Screen, screen.MakeItems([]screen.ItemSpec{
		{ &iHelp2, 2, false },
		{ &iEnd, 4, false },
	}))

	helpMain := help{ "q: back", true }

	menu := &matchMenu

	mBoard := match.Board
	turn := match.Turn
	endWinner := board.TWhite

	draw := func() {
		state.Screen.Tc.Clear()

		state.DrawHeader()

		if menu == &matchMenu {
			state.Screen.DrawHelp(0, iHelp1.Start, state.Screen.Styles.Help,
				state.Screen.Styles.HelpBold, makeHelp(helpMain))

			state.Screen.DrawBoard(mBoard, board.TWhite, 5, iBoard.Start)
			state.Screen.DrawTurns(match.White, match.Black,
				(turn == board.TWhite), board.TWhite, iTurn.Start)
		} else if menu == &endMenu {
			state.Screen.DrawHelp(0, iHelp2.Start, state.Screen.Styles.Help,
				state.Screen.Styles.HelpBold, makeHelp(helpMain))

			if endWinner == board.TWhite {
				state.Screen.DrawText(5, iEnd.Start,
					state.Screen.Styles.Normal, "WINNER: " +
					parse.DisplayUser(match.White.Name, match.White.Guest))
			} else {
				state.Screen.DrawText(5, iEnd.Start,
					state.Screen.Styles.Normal, "WINNER: " +
					parse.DisplayUser(match.Black.Name, match.Black.Guest))
			}
			state.Screen.DrawText(5, iEnd.Start + 2,
				state.Screen.Styles.Normal, "Waiting for players...")
		}

		menu.Draw()

		state.Screen.Tc.Show()
	}

	state.Screen.Tc.SetStyle(state.Screen.Styles.Normal)
	draw()

	loop := true
	for loop {
		e := pollMenu(menu, state)

		switch e := e.(type) {
		case *tcell.EventKey:
			if e.Key() == tcell.KeyRune && e.Rune() == 'q' {
				id := state.SendMsg(consts.ClientWatchQuit)
				state.Wait(func(msg protocol.Msg) {
					errstr, good := gamestate.CheckResponse(msg, id)
					if !good { return }

					if errstr != "" && errstr != "ctx" {
						ErrMsgMenu(state, consts.ClientWatchQuit, errstr)
					}

					state.Unwait()
					loop = false
				})
			}

		case *tcell.EventResize:
			draw()

		case protocol.Msg:
			failInvalidMsg(state, e)
			if !state.ApplyMsg(e) {
				switch msg := e.(type) {
				case protocol.MsgWatchBegin:
					match = msg
					mBoard = match.Board
					turn = match.Turn
					menu = &matchMenu

				case protocol.MsgWatchEnd:
					endWinner = msg.Team
					menu = &endMenu

				case protocol.MsgWatchTurn:
					turn = msg.Team

				case protocol.MsgWatchMove:
					for _, move := range msg.Moves {
						mBoard.MoveDirty(move.X1, move.Y1, move.X2, move.Y2, move.Promote)
					}

				case protocol.MsgWatchDestroy:
					NotifyMenu(state, "The game ended.")
					return
				}

				draw()
			}

		}
	}
}
