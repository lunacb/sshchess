package menus

import  (
	"github.com/gdamore/tcell/v2"

	"codeberg.org/lunacb/sshchess/cmd/sshchess/gamestate"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/protocol"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/screen"
	"codeberg.org/lunacb/sshchess/lib/consts"
)

type WaitmatchResult int
const (
	WaitmatchGood WaitmatchResult = iota
	WaitmatchCtx
	WaitmatchQuit
)

func waitmatchMenu(state *gamestate.State, waitMsg string, sendMsg ...string) WaitmatchResult {
	var iHelp, iTitle *screen.Item

	menu := screen.NewMenu(&state.Screen, screen.MakeItems([]screen.ItemSpec{
		{ &iHelp, 2, false },
		{ &iTitle, 2, false },
	}))

	helpMain := help{ "q: back", true }

	draw := func() {
		state.Screen.Tc.Clear()

		state.DrawHeader()

		state.Screen.DrawHelp(0, iHelp.Start, state.Screen.Styles.Help, state.Screen.Styles.HelpBold,
			makeHelp(helpMain))
		state.Screen.DrawText(5, iTitle.Start, state.Screen.Styles.Normal, waitMsg)

		menu.Draw()

		state.Screen.Tc.Show()
	}

	state.Screen.Tc.SetStyle(state.Screen.Styles.Normal)
	draw()

	res := WaitmatchGood

	loop := true

	id := state.SendMsg(sendMsg...)
	state.Wait(func(msg protocol.Msg) {
		errstr, good := gamestate.CheckResponse(msg, id)
		if !good { return }

		if errstr != "" {
			if errstr == "ctx" {
				res = WaitmatchCtx
				loop = false
			} else {
				ErrMsgMenu(state, sendMsg[0], errstr)
			}
		}

		state.Unwait()
	})

	for loop {
		e := pollMenu(&menu, state)

		switch e := e.(type) {
		case *tcell.EventKey:
			if e.Key() == tcell.KeyRune && e.Rune() == 'q' {
				id := state.SendMsg(consts.ClientWaitingQuit)
				state.Wait(func(msg protocol.Msg) {
					errstr, good := gamestate.CheckResponse(msg, id)
					if !good { return }

					if errstr != "" {
						if errstr == "ctx" {
							state.Unwait()
							loop = false
							return
						}

						ErrMsgMenu(state, consts.ClientWaitingQuit, errstr)
					}

					state.Unwait()
					loop = false
				})
				return WaitmatchGood
			}

		case *tcell.EventResize:
			draw()

		case protocol.Msg:
			failInvalidMsg(state, e)
			if !state.ApplyMsg(e) {
				switch msg := e.(type) {
				case protocol.MsgWaitingQuit:
					return WaitmatchQuit
				case protocol.MsgGameBegin:
					MatchMenu(state, msg)
					return WaitmatchGood
				}
			}
		}
	}

	return res
}

func RandomMatchMenu(state *gamestate.State) {
	waitmatchMenu(state, "Waiting for an opponent...", consts.ClientRandomMatch)
}
