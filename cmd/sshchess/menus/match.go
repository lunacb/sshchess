package menus

import  (
	"github.com/gdamore/tcell/v2"

	"codeberg.org/lunacb/sshchess/cmd/sshchess/gamestate"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/protocol"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/screen"
	"codeberg.org/lunacb/sshchess/lib/board"
	"codeberg.org/lunacb/sshchess/lib/consts"
	"codeberg.org/lunacb/sshchess/lib/parse"
)

func parseMove(str string) (int, int, int, int, bool) {
	if len(str) != 4 { return 0, 0, 0, 0, false }
	var sx, sy, dx, dy int

	runes := []rune(str)

	if runes[0] >= 'a' && runes[0] <= 'h' {
		sx = int(runes[0]) - 'a' + 1
	} else {
		return 0, 0, 0, 0, false
	}

	if runes[1] >= '1' && runes[1] <= '8' {
		sy = int(runes[1]) - '1' + 1
	} else {
		return 0, 0, 0, 0, false
	}

	if runes[2] >= 'a' && runes[2] <= 'h' {
		dx = int(runes[2]) - 'a' + 1
	} else {
		return 0, 0, 0, 0, false
	}

	if runes[3] >= '1' && runes[3] <= '8' {
		dy = int(runes[3]) - '1' + 1
	} else {
		return 0, 0, 0, 0, false
	}

	return 7 - (sy - 1), sx - 1, 7 - (dy - 1), dx - 1, true
}

func gameEnd(state *gamestate.State, winner protocol.User) (protocol.MsgGameBegin, bool) {
	var iWinner, iWaitWinner, iTitle, iWaitTitle, iYes, iNo *screen.Item

	askMenu := screen.NewMenu(&state.Screen, screen.MakeItems([]screen.ItemSpec{
		{ &iWinner, 2, false },
		{ &iTitle, 2, false },
		{ &iYes, 2, true },
		{ &iNo, 2, true },
	}))

	waitMenu := screen.NewMenu(&state.Screen, screen.MakeItems([]screen.ItemSpec{
		{ &iWaitWinner, 3, false },
		{ &iWaitTitle, 3, false },
	}))

	menu := &askMenu

	title := "Rematch?"
	winnerStr := "WINNER: " + parse.DisplayUser(winner.Name, winner.Guest)
	draw := func() {
		state.Screen.Tc.Clear()

		state.DrawHeader()

		if menu == &askMenu {
			state.Screen.DrawText(5, iWinner.Start, state.Screen.Styles.Normal, winnerStr)
			state.Screen.DrawText(5, iTitle.Start, state.Screen.Styles.Normal, title)
			state.Screen.DrawText(5, iYes.Start, state.Screen.Styles.Normal, "Yes")
			state.Screen.DrawText(5, iNo.Start, state.Screen.Styles.Normal, "No")
		} else {
			state.Screen.DrawText(5, iWaitWinner.Start, state.Screen.Styles.Normal, winnerStr)
			state.Screen.DrawText(5, iWaitTitle.Start, state.Screen.Styles.Normal, title)
			state.Screen.DrawText(5, iWaitTitle.Start + 2,
				state.Screen.Styles.Normal, "Waiting for opponent...")
		}

		menu.Draw()

		state.Screen.Tc.Show()
	}

	state.Screen.Tc.SetStyle(state.Screen.Styles.Normal)
	draw()

	loop := true
	for loop {
		e := pollMenu(menu, state)

		switch e := e.(type) {
		case *tcell.EventKey:
			if e.Key() == tcell.KeyRune && e.Rune() == 'q' {
				id := state.SendMsg(consts.ClientGameQuit)
				state.Wait(func(msg protocol.Msg) {
					errstr, good := gamestate.CheckResponse(msg, id)
					if !good { return }

					if errstr != "" {
						ErrMsgMenu(state, consts.ClientGameQuit, errstr)
					}

					loop = false
					state.Unwait()
				})
			}

		case *tcell.EventResize:
			draw()

		case *screen.Item:
			switch e {
			case iYes:
				id := state.SendMsg(consts.ClientGameRematch)
				state.Wait(func(msg protocol.Msg) {
					errstr, good := gamestate.CheckResponse(msg, id)
					if !good { return }

					if errstr != "" {
						if errstr == "ctx" {
							state.Unwait()
							return
						}

						ErrMsgMenu(state, consts.ClientGameRematch, errstr)
					}

					state.Unwait()
					menu = &waitMenu
				})

			case iNo:
				id := state.SendMsg(consts.ClientGameQuit)
				state.Wait(func(msg protocol.Msg) {
					errstr, good := gamestate.CheckResponse(msg, id)
					if !good { return }

					if errstr != "" {
						ErrMsgMenu(state, consts.ClientGameQuit, errstr)
					}

					loop = false
					state.Unwait()
				})
			}
			
			draw()

		case protocol.Msg:
			failInvalidMsg(state, e)
			if !state.ApplyMsg(e) {
				switch msg := e.(type) {
				case protocol.MsgGameWantRematch:
					title = "Accept rematch?"

				case protocol.MsgGameBegin:
					return msg, true

				case protocol.MsgGameAlone:
					id := state.SendMsg(consts.ClientGameQuit)
					state.Wait(func(msg protocol.Msg) {
						errstr, good := gamestate.CheckResponse(msg, id)
						if !good { return }

						if errstr != "" {
							ErrMsgMenu(state, consts.ClientGameQuit, errstr)
						}

						state.Unwait()
						loop = false
						NotifyMenu(state, "Opponent left.")
					})
				}
			}
			draw()
		}
	}

	return protocol.MsgGameBegin{}, false
}

func choosePromote(state *gamestate.State) board.PieceName {
	var iTitle, iQueen, iRook, iBishop, iKnight *screen.Item

	menu := screen.NewMenu(&state.Screen, screen.MakeItems([]screen.ItemSpec{
		{ &iTitle, 2, false  },
		{ &iQueen, 1, true  },
		{ &iRook, 1, true  },
		{ &iBishop, 1, true  },
		{ &iKnight, 1, true  },
	}))

	draw := func() {
		state.Screen.Tc.Clear()

		state.DrawHeader()

		state.Screen.DrawText(5, iTitle.Start, state.Screen.Styles.Normal, "Promote pawn to:")
		state.Screen.DrawText(5, iQueen.Start, state.Screen.Styles.Normal, "Queen")
		state.Screen.DrawText(5, iRook.Start, state.Screen.Styles.Normal, "Rook")
		state.Screen.DrawText(5, iBishop.Start, state.Screen.Styles.Normal, "Bishop")
		state.Screen.DrawText(5, iKnight.Start, state.Screen.Styles.Normal, "Knight")

		menu.Draw()

		state.Screen.Tc.Show()
	}

	state.Screen.Tc.SetStyle(state.Screen.Styles.Normal)
	draw()

	loop := true
	for loop {
		e := pollMenu(&menu, state)

		switch e := e.(type) {
		case *screen.Item:
			switch e {
			case iQueen: return board.PQueen
			case iRook: return board.PRook
			case iBishop: return board.PBishop
			case iKnight: return board.PKnight
			}

		case protocol.Msg:
			failInvalidMsg(state, e)
			state.ApplyMsg(e)
		}
	}

	return board.PNone
}

func match(state *gamestate.State, matchMsg protocol.MsgGameBegin) (protocol.MsgGameBegin, bool) {
	var iHelp, iBoard, iTurn, iMove, iErr *screen.Item

	menu := screen.NewMenu(&state.Screen, screen.MakeItems([]screen.ItemSpec{
		{ &iHelp, 2, false },
		{ &iBoard, 19, false },
		{ &iTurn, 3, false },
		{ &iMove, 2, false },
		{ &iErr, 2, false },
	}))
	tMove := screen.NewTextboxMap(&menu, iMove, 5 + len("Move "), false,
		screen.ChessRunes, []int{0, 1, 2 + len(" To "), 3 + len(" To "), 4 + len(" To ")})
	tMove.ShowCursor()
	defer tMove.HideCursor()

	helpMain := help{ "q: leave match, \"a2a3\": move piece at a2 to a3", true }

	mBoard := board.NewBoard()
	myMove := (matchMsg.Team == board.TWhite)
	errMsg := ""

	draw := func() {
		state.Screen.Tc.Clear()

		state.DrawHeader()

		state.Screen.DrawHelp(0, iHelp.Start, state.Screen.Styles.Help, state.Screen.Styles.HelpBold,
			makeHelp(helpMain))
		state.Screen.DrawBoard(mBoard, matchMsg.Team, 5, iBoard.Start)
		state.Screen.DrawTurns(state.User, matchMsg.Opponent, myMove, matchMsg.Team, iTurn.Start)
		state.Screen.DrawText(5, iMove.Start, state.Screen.Styles.Normal, "Move    To   ")
		state.Screen.DrawText(5, iErr.Start, state.Screen.Styles.Error, errMsg)
		tMove.Draw()

		menu.Draw()

		state.Screen.Tc.Show()
	}

	state.Screen.Tc.SetStyle(state.Screen.Styles.Normal)
	draw()

	loop := true
	for loop {
		e := pollTextbox(&tMove, state)

		switch e := e.(type) {
		case pollUserNext:
			if myMove {
				sx, sy, dx, dy, good := parseMove(tMove.Text)
				tMove.Text = ""
				tMove.ShowCursor()
				draw()
				if !good {
					errMsg = "* Invalid format."
					draw()
					continue
				}

				move, good := mBoard.FindMoveFromPos(matchMsg.Team, sx, sy, dx, dy)
				if !good {
					errMsg = "* Invalid move."
					draw()
					continue
				}

				switch piece := move.Piece.(type) {
				case board.MovePawn:
					if board.PawnNeedsPromote(matchMsg.Team, dx) {
						piece.Promote = choosePromote(state)
						move.Piece = piece
					}
				}

				var bmove board.MoveResults
				if bmove, good = mBoard.MoveValid(matchMsg.Team, move); !good {
					errMsg = "* Invalid move."
					draw()
					continue
				}

				errMsg = ""
				draw()

				id := state.SendMsg(consts.ClientGameMove, move.MakeMsg())
				state.Wait(func(msg protocol.Msg) {
					errstr, good := gamestate.CheckResponse(msg, id)
					if !good { return }

					if errstr != "" {
						if errstr == "ctx" {
							state.Unwait()
							return
						}

						ErrMsgMenu(state, consts.ClientGameMove, errstr)
					}

					mBoard.Move(bmove)
					draw()

					state.Unwait()
					myMove = false
				})
			}

		case *tcell.EventKey:
			if e.Key() == tcell.KeyRune && e.Rune() == 'q' {
				if QuestionMenu(state, "Are you sure you want to quit?") {
					id := state.SendMsg(consts.ClientGameQuit)
					state.Wait(func(msg protocol.Msg) {
						errstr, good := gamestate.CheckResponse(msg, id)
						if !good { return }

						if errstr != "" {
							ErrMsgMenu(state, consts.ClientGameQuit, errstr)
						}

						loop = false
						state.Unwait()
					})
				}
				draw()
			}

		case *tcell.EventResize:
			draw()

		case protocol.Msg:
			failInvalidMsg(state, e)
			if !state.ApplyMsg(e) {
				switch msg := e.(type) {
				case protocol.MsgGameMove:
					for _, move := range msg.Moves {
						mBoard.MoveDirty(move.X1, move.Y1, move.X2, move.Y2, move.Promote)
					}
					draw()

				case protocol.MsgGameTurn:
					myMove = true

				case protocol.MsgGameEnd:
					tMove.HideCursor()
					if msg.Team == matchMsg.Team {
						return gameEnd(state, state.User)
					} else {
						return gameEnd(state, matchMsg.Opponent)
					}

				case protocol.MsgGameAlone:
					id := state.SendMsg(consts.ClientGameQuit)
					state.Wait(func(msg protocol.Msg) {
						errstr, good := gamestate.CheckResponse(msg, id)
						if !good { return }

						if errstr != "" {
							ErrMsgMenu(state, consts.ClientGameQuit, errstr)
						}

						loop = false
						state.Unwait()
						NotifyMenu(state, "Opponent left.")
					})
				}
			}
			draw()
		}
	}

	return protocol.MsgGameBegin{}, false
}

func MatchMenu(state *gamestate.State, matchMsg protocol.MsgGameBegin) {
	good := true
	for good {
		matchMsg, good = match(state, matchMsg)
	}
}
