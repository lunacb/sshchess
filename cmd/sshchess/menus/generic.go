package menus

import  (
	"github.com/gdamore/tcell/v2"

	"fmt"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/gamestate"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/exit"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/protocol"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/screen"
)

func NotifyMenu(state *gamestate.State, message string, a ...interface{}) {
	var iTitle, iOk *screen.Item

	{
		if state.Screen.CursorVisible {
			x, y := state.Screen.CursorX, state.Screen.CursorY
			state.Screen.HideCursor()
			defer state.Screen.ShowCursor(x, y)
		}
		if state.KeyGrab != nil {
			grab := state.KeyGrab
			state.KeyGrab = nil
			defer func() { state.KeyGrab = grab }()
		}
	}
	
	message = fmt.Sprintf(message, a...)

	menu := screen.NewMenu(&state.Screen, screen.MakeItems([]screen.ItemSpec{
		{ &iTitle, 2, false },
		{ &iOk, 2, true },
	}))

	draw := func() {
		state.Screen.Tc.Clear()

		state.DrawHeader()

		state.Screen.DrawText(5, iTitle.Start, state.Screen.Styles.Normal, message)
		state.Screen.DrawText(5, iOk.Start, state.Screen.Styles.Normal, "Ok")

		menu.Draw()

		state.Screen.Tc.Show()
	}

	state.Screen.Tc.SetStyle(state.Screen.Styles.Normal)
	draw()

	loop := true
	for loop {
		e := pollMenu(&menu, state)

		switch e := e.(type) {
		case *screen.Item:
			switch e {
			case iOk:
				return
			}

		case *tcell.EventResize:
			draw()

		case protocol.Msg:
			failInvalidMsg(state, e)
			state.ApplyMsg(e)
		}
	}
}

func QuestionMenu(state *gamestate.State, message string, a ...interface{}) bool {
	var iTitle, iYes, iNo *screen.Item

	{
		if state.Screen.CursorVisible {
			x, y := state.Screen.CursorX, state.Screen.CursorY
			state.Screen.HideCursor()
			defer state.Screen.ShowCursor(x, y)
		}
		if state.KeyGrab != nil {
			grab := state.KeyGrab
			state.KeyGrab = nil
			defer func() { state.KeyGrab = grab }()
		}
	}
	
	message = fmt.Sprintf(message, a...)

	menu := screen.NewMenu(&state.Screen, screen.MakeItems([]screen.ItemSpec{
		{ &iTitle, 2, false },
		{ &iYes, 1, true },
		{ &iNo, 1, true },
	}))

	draw := func() {
		state.Screen.Tc.Clear()

		state.DrawHeader()

		state.Screen.DrawText(5, iTitle.Start, state.Screen.Styles.Normal, message)
		state.Screen.DrawText(5, iYes.Start, state.Screen.Styles.Normal, "Yes")
		state.Screen.DrawText(5, iNo.Start, state.Screen.Styles.Normal, "No")

		menu.Draw()

		state.Screen.Tc.Show()
	}

	state.Screen.Tc.SetStyle(state.Screen.Styles.Normal)
	draw()

	loop := true
	for loop {
		e := pollMenu(&menu, state)

		switch e := e.(type) {
		case *screen.Item:
			switch e {
			case iYes:
				return true
			case iNo:
				return false
			}

		case *tcell.EventResize:
			draw()

		case protocol.Msg:
			failInvalidMsg(state, e)
			state.ApplyMsg(e)
		}
	}

	return false
}

func FatalMenu(state *gamestate.State, message string, a ...interface{}) {
	NotifyMenu(state, message, a...)
	exit.Exit(1, state)
}

func ErrMsgMenu(state *gamestate.State, sent string, errstr string) {
	FatalMenu(state, "Server Error: Sent '%s', got '%s'", sent, errstr)
}
