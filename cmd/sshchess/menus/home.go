package menus

import  (
	"github.com/gdamore/tcell/v2"

	"codeberg.org/lunacb/sshchess/cmd/sshchess/gamestate"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/protocol"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/screen"
)

func HomeMenu(state *gamestate.State) {
	var iHelp, iMatch, iSearch, iHighscores, iGames, iChallenges, iAbout *screen.Item

	menu := screen.NewMenu(&state.Screen, screen.MakeItems([]screen.ItemSpec{
		{ &iHelp, 2, false },
		{ &iMatch, 2, true },
		{ &iSearch, 2, true },
		{ &iChallenges, 7, true },
		{ &iHighscores, 7, true },
		{ &iGames, 7, true },
		{ &iAbout, 2, true },
	}))

	helpMain := help{ "q: quit", true }

	draw := func() {
		state.Screen.Tc.Clear()

		state.DrawHeader()

		state.Screen.DrawHelp(0, iHelp.Start, state.Screen.Styles.Help, state.Screen.Styles.HelpBold,
			makeHelp(helpMain))
		state.Screen.DrawText(5, iMatch.Start, state.Screen.Styles.Normal, "Random Match")
		state.Screen.DrawText(5, iSearch.Start, state.Screen.Styles.Normal, "Search and Challenge Users")
		state.Screen.DrawText(5, iChallenges.Start, state.Screen.Styles.Normal, "Challenges")
		state.Screen.DrawText(5, iHighscores.Start, state.Screen.Styles.Normal, "Weekly Highscores")
		state.Screen.DrawText(5, iGames.Start, state.Screen.Styles.Normal, "Watch Active Games")
		state.Screen.DrawText(5, iAbout.Start, state.Screen.Styles.Normal, "About")

		{
			users := [][]protocol.User{}
			for _, v := range state.Challenges {
				users = append(users, []protocol.User{v})
			}
			table := screen.TableFromUsers(users)
			table = screen.TableWithLength(table, 1, 3)
			state.Screen.DrawTable(6, iChallenges.Start + 1,
				state.Screen.Styles.Column, state.Screen.Styles.Normal, table, "Name")
		}
		{
			users := [][]protocol.UserStat{}
			for _, v := range state.Weekly {
				users = append(users, []protocol.UserStat{v})
			}
			table := screen.TableFromUserStats(users)
			table = screen.TableWithLength(table, 3, 3)
			state.Screen.DrawTable(6, iHighscores.Start + 1,
			state.Screen.Styles.Column, state.Screen.Styles.Normal, table, "Name", "Wins", "Losses")
		}
		{
			users := [][]protocol.User{}
			for _, v := range state.Matches {
				users = append(users, []protocol.User{v.One, v.Two})
			}
			table := screen.TableFromUsers(users)
			table = screen.TableWithLength(table, 2, 3)
			state.Screen.DrawTable(6, iGames.Start + 1,
				state.Screen.Styles.Column, state.Screen.Styles.Normal, table, "Name", "Name")
		}

		menu.Draw()

		state.Screen.Tc.Show()
	}

	state.Screen.Tc.SetStyle(state.Screen.Styles.Normal)
	draw()

	loop := true
	for loop {
		e := pollMenu(&menu, state)

		switch e := e.(type) {
		case *tcell.EventKey:
			if e.Key() == tcell.KeyRune && e.Rune() == 'q' {
				return
			}

		case *tcell.EventResize:
			draw()

		case *screen.Item:
			switch e {
			case iMatch:
				RandomMatchMenu(state)
			case iHighscores:
				HighscoreMenu(state)
			case iChallenges:
				ChallengesMenu(state)
			case iSearch:
				SearchMenu(state)
			case iGames:
				MatchesMenu(state)
			case iAbout:
				AboutMenu(state)
			}
			draw()

		case protocol.Msg:
			failInvalidMsg(state, e)
			state.ApplyMsg(e)
			draw()
		}
	}
}
