package menus

import  (
	"github.com/gdamore/tcell/v2"

	"codeberg.org/lunacb/sshchess/cmd/sshchess/gamestate"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/protocol"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/screen"
	"codeberg.org/lunacb/sshchess/lib/consts"
)

// TODO: make some common functions for things like this

func MatchesMenu(state *gamestate.State) {
	var iHelp, iTitle, iGap *screen.Item
	var iMatches [pageSize]*screen.Item

	var items []screen.Item
	{
		spec := make([]screen.ItemSpec, 0, 11)
		spec = append(spec, screen.ItemSpec{ &iHelp, 2, false },
			screen.ItemSpec{ &iTitle, 2, false },
			screen.ItemSpec{ &iGap, 1, false })
		for i, _ := range iMatches {
			spec = append(spec, screen.ItemSpec{ &iMatches[i], 1, true })
		}
		items = screen.MakeItems(spec)
	}
	menu := screen.NewMenu(&state.Screen, items)

	page := 0
	nPages := len(state.Matches) / pageSize
	if len(state.Matches) % pageSize != 0 || len(state.Matches) == 0 { nPages += 1 }

	helpMain := help{ "q: back, Enter: watch", true }
	helpNext := help{ "n: next", (nPages > 1) }
	helpPrev := help{ "p: prev", false }

	draw := func() {
		state.Screen.Tc.Clear()

		state.DrawHeader()

		state.Screen.DrawHelp(0, iHelp.Start, state.Screen.Styles.Help, state.Screen.Styles.HelpBold,
			makeHelp(helpMain))
		state.Screen.DrawText(5, iTitle.Start, state.Screen.Styles.Normal, "Matches:")
		{
			list := state.Matches[page*pageSize:]
			if len(list) > pageSize { list = list[:pageSize] }

			users := [][]protocol.User{}
			for _, v := range list {
				users = append(users, []protocol.User{v.One, v.Two})
			}
			table := screen.TableFromUsers(users)
			state.Screen.DrawTable(6, iGap.Start, state.Screen.Styles.Column,
				state.Screen.Styles.Normal, table, "Name", "Name")
			menu.Items = items[:len(list)]
		}

		menu.Draw()

		state.Screen.Tc.Show()
	}

	state.Screen.Tc.SetStyle(state.Screen.Styles.Normal)
	draw()

	loop := true
	for loop {
		e := pollMenu(&menu, state)

		switch e := e.(type) {
		case *tcell.EventKey:
			key := e.Key()
			if key == tcell.KeyRune {
				krune := e.Rune()
				if krune == 'q' {
					return
				}

				if krune == 'n' {
					if page + 1 < nPages {
						page += 1
						helpPrev.on = true
						if page + 1 == nPages {
							helpNext.on = false
						}
					}
					draw()
				} else if krune == 'p' {
					if page != 0 {
						page -= 1
						helpNext.on = true
						if page == 0 {
							helpPrev.on = false
						}
					}
					draw()
				}
			}

		case *tcell.EventResize:
			draw()

		case *screen.Item:
			for i, v := range iMatches {
				if v == e {
					match := state.Matches[page*pageSize+i]

					// TODO: make a common function for this
					oneGuest, twoGuest := "", ""
					if match.One.Guest {
						oneGuest = "guest"
					} else {
						oneGuest = "user"
					}

					if match.Two.Guest {
						twoGuest = "guest"
					} else {
						twoGuest = "user"
					}

					id := state.SendMsg(consts.ClientWatchMatch, oneGuest, match.One.Name, twoGuest, match.Two.Name)

					gotOk := false
					state.Wait(func(msg protocol.Msg) {
						if !gotOk {
							errstr, good := gamestate.CheckResponse(msg, id)
							if !good { return }

							if errstr == "" {
								gotOk = true
								return
							} else {
								if errstr == "ctx" {
									state.Unwait()
									return
								}

								ErrMsgMenu(state, consts.ClientWatchMatch, errstr)
							}
						} else {
							switch msg := msg.(type) {
							case protocol.MsgWatchBegin:
								state.Unwait()
								WatchMenu(state, msg)
							}
						}
					})
				}
			}
			draw()

		case protocol.Msg:
			failInvalidMsg(state, e)
			state.ApplyMsg(e)
			nPages = len(state.Matches) / pageSize
			if len(state.Matches) % pageSize != 0 || len(state.Matches) == 0 { nPages += 1 }
			if page >= nPages {page = nPages - 1 }
			helpNext.on = (page + 1 != nPages)
			helpPrev.on = (page != 0)
			draw()
		}
	}
}
