package menus

import (
	"github.com/gdamore/tcell/v2"

	"codeberg.org/lunacb/sshchess/cmd/sshchess/exit"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/gamestate"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/protocol"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/screen"
)

type pollUserExit struct{}
type pollUserNext struct{}

type pollResults interface{}

type help struct {
	str string
	on bool
}


const pageSize int = 10
const minScreenWidth int = 64
const minScreenHeight int = 32

func waitResize(state *gamestate.State, x int, y int) {
	for {
		if x >= minScreenWidth && y >= minScreenHeight {
			return
		}

		state.Screen.Tc.Clear()
		state.Screen.DrawText(0, 0, state.Screen.Styles.Normal, "Window too small!")
		state.Screen.Tc.Show()

		e := <-state.TcCh
		switch e := e.(type) {
		case *tcell.EventKey:
			if e.Key() == tcell.KeyCtrlC {
				exit.Exit(0, state)
			}
		case *tcell.EventResize:
			x, y = e.Size()
		}
	}
}

// TODO: handle reszie, etc. here
func doTextBox(tbox *screen.Textbox, menu *screen.Menu, state *gamestate.State, draw func()) bool {
	tbox.ShowCursor()
	defer tbox.HideCursor()

	for {
		e := pollTextbox(tbox, state)

		switch e := e.(type) {
		case pollUserExit:
			_ = e
			return false

		case pollUserNext:
			_ = e
			return true

		case *tcell.EventResize:
			draw()

		case protocol.Msg:
			failInvalidMsg(state, e)
			state.ApplyMsg(e)
		}
	}

	return false
}

func failInvalidMsg(state *gamestate.State, msg protocol.Msg) {
	switch msg := msg.(type) {
	case protocol.MsgInvalid:
		// TODO: document the specific message
		FatalMenu(state, "Invalid message from the server: %s", msg.Msg)
	}
}

func pollMenu(m *screen.Menu, state *gamestate.State) pollResults {
	if m.QueuedFocus != -1 {
		m.SetArrowVisible(false)
		m.Pos = m.QueuedFocus
		m.QueuedFocus = -1
		m.S.Tc.Show()
		return &m.Items[m.Pos]
	}

	m.SetArrowVisible(true)
	m.S.Tc.Show()

	if m.QueuedSelect != -1 {
		m.SetArrowVisible(false)
		m.Pos = m.QueuedSelect
		m.QueuedSelect = -1
		m.SetArrowVisible(true)
		m.S.Tc.Show()
	}

	select {
	case e := <-state.TcCh:
		m.SetArrowVisible(false)
		switch e := e.(type) {
		case *tcell.EventKey:
			if state.KeyGrab != nil { return nil }
			switch key := e.Key(); key {
			case tcell.KeyCtrlC:
				exit.Exit(0, state)

			case tcell.KeyDown:
				if m.Pos < len(m.Items) - 1 {
					m.Pos += 1
				}

			case tcell.KeyUp:
				if(m.Pos > 0) {
					m.Pos -= 1
				}

			case tcell.KeyEnter:
				m.S.Tc.Show()
				if len(m.Items) == 0 {
					return nil
				} else {
					return &m.Items[m.Pos]
				}

			case tcell.KeyRune:
				krune := e.Rune()
				if krune >= '0' && krune <= '9' {
					var i int
					if krune == '0' {
						i = 9
					} else {
						i = int(krune) - '1'
					}
					if i < len(m.Items) {
						m.Pos = i
					}
				} else if krune == 'j' {
					if m.Pos < len(m.Items) - 1 {
						m.Pos += 1
					}
				} else if krune == 'k' {
					if m.Pos > 0 {
						m.Pos -= 1
					}
				} else {
					return e
				}

			default:
				return e
			}

		case *tcell.EventResize:
			x, y := e.Size()
			waitResize(state, x, y)
			return e
		}
		m.SetArrowVisible(true)
		m.S.Tc.Show()

	case e := <-state.SockCh:
		return e

	case e := <-state.ErrCh:
		FatalMenu(state, "Socket error: %s", e)
	}

	return nil
}

func pollTextbox(t *screen.Textbox, state *gamestate.State) pollResults {
	select {
	case e := <-state.TcCh:
		switch e := e.(type) {
		case *tcell.EventKey:
			if state.KeyGrab != nil { return nil }
			switch key := e.Key(); key {
			case tcell.KeyCtrlC:
				exit.Exit(0, state)

			case tcell.KeyEscape:
				return pollUserExit{}

			case tcell.KeyEnter:
				return pollUserNext{}

			case tcell.KeyBackspace, tcell.KeyBackspace2:
				if len(t.Text) != 0 {
					t.Text = t.Text[:len(t.Text)-1]
					t.S.Tc.SetContent(t.GetX(len(t.Text)), t.I.Start, ' ', nil, t.S.Styles.Normal)
					t.S.ShowCursor(t.GetX(len(t.Text)), t.I.Start)
					t.S.Tc.Show()
				}

			case tcell.KeyRune:
				krune := e.Rune()
				if t.Runes(krune) {
					if len(t.Text) < t.MaxLen {
						srune := krune
						if t.Pass {
							srune = '*'
						}
						t.S.Tc.SetContent(t.GetX(len(t.Text)), t.I.Start, srune, nil, t.S.Styles.Input)
						t.Text += string(krune)
						t.S.ShowCursor(t.GetX(len(t.Text)), t.I.Start)
						t.S.Tc.Show()
					}
				} else {
					return e
				}

			default:
				return e
			}

		case *tcell.EventResize:
			x, y := e.Size()
			waitResize(state, x, y)
			return e
		}

	case e := <-state.SockCh:
		return e

	case e := <-state.ErrCh:
		FatalMenu(state, "Socket error: %s", e)
	}

	return nil
}

func makeHelp(list ...help) string {
	if len(list) == 0 { return "()" }

	str := "("
	for _, v := range list {
		if !v.on { continue }

		if len(str) != 1  {
			str  += ", "
		}
		str += v.str
	}
	str += ")"

	return str
}
