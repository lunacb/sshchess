package menus

import  (
	"github.com/gdamore/tcell/v2"

	"codeberg.org/lunacb/sshchess/cmd/sshchess/gamestate"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/protocol"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/screen"
	"codeberg.org/lunacb/sshchess/lib/consts"
)

func login(state *gamestate.State) (protocol.User, bool) {
	var iHelp, iUser, iPass, iLogin, iErr *screen.Item

	menu := screen.NewMenu(&state.Screen, screen.MakeItems([]screen.ItemSpec{
		{ &iHelp, 2, false },
		{ &iUser, 2, true },
		{ &iPass, 2, true },
		{ &iLogin, 1, true },
		{ &iErr, 3, false },
	}))
	menu.QueueFocus(iUser)

	helpMain := help{ "q: quit", true }
	helpText := help{ "Esc: leave textbox", false }

	loginErr := ""

	tUser := screen.NewTextbox(&menu, iUser, 5 + len("Username: "), false,
		screen.GraphRunes, consts.MaxUsernameLen)
	tPass := screen.NewTextbox(&menu, iPass, 5 + len("Password: "), true,
		screen.GraphRunes, consts.MaxPasswordLen)

	draw := func() {
		state.Screen.Tc.Clear()

		state.DrawHeader()

		state.Screen.DrawHelp(0, iHelp.Start, state.Screen.Styles.Help, state.Screen.Styles.HelpBold,
			makeHelp(helpMain, helpText))
		state.Screen.DrawText(5, iUser.Start, state.Screen.Styles.Normal, "Username:")
		state.Screen.DrawText(5, iPass.Start, state.Screen.Styles.Normal, "Password:")
		state.Screen.DrawText(5, iLogin.Start, state.Screen.Styles.Normal, "Login")
		state.Screen.DrawText(1, iErr.Start, state.Screen.Styles.Error, loginErr)
		tUser.Draw()
		tPass.Draw()
		menu.Draw()

		state.Screen.Tc.Show()
	}

	state.Screen.Tc.SetStyle(state.Screen.Styles.Normal)
	draw()

	resUser := protocol.User{}
	resGood := false
	loop := true
	for loop {
		e := pollMenu(&menu, state)

		switch e := e.(type) {
		case *tcell.EventKey:
			if e.Key() == tcell.KeyRune && e.Rune() == 'q' {
				return protocol.User{}, false
			}

		case *tcell.EventResize:
			draw()

		case *screen.Item:
			switch e {
			case iUser:
				helpMain.on, helpText.on = false, true
				draw()
				if doTextBox(&tUser, &menu, state, draw) {
					menu.QueueFocus(iPass)
				}
				helpMain.on, helpText.on = true, false

			case iPass:
				helpMain.on, helpText.on = false, true
				draw()
				if doTextBox(&tPass, &menu, state, draw) {
					menu.QueueSelect(iLogin)
				}
				helpMain.on, helpText.on = true, false

			case iLogin:
				if len(tUser.Text) >= 1 && len(tPass.Text) >= 1 {
					id := state.SendMsg(consts.ClientLogin, "user", tUser.Text, tPass.Text)
					username := tUser.Text
					state.Wait(func(msg protocol.Msg) {
						errstr, good := gamestate.CheckResponse(msg, id)
						if !good { return }

						if errstr == "" {
							resUser = protocol.User{ Name: username, Guest: false }
							resGood = true
							loop = false
						} else {
							if errstr == "account" {
								loginErr = "* Incorrect username or password."
							} else if errstr == "exists" {
								loginErr = "* Someone is already logged into this account."
							} else {
								state.Unwait()
								ErrMsgMenu(state, consts.ClientLogin, errstr)
							}
						}
						state.Unwait()
					})
				} else {
					if len(tUser.Text) < 1 {
						loginErr = "* Username must be at least 1 character."
					} else {
						loginErr = "* Password must be at least 1 characters."
					}
				}
			}
			draw()

		case protocol.Msg:
			failInvalidMsg(state, e)
			state.ApplyMsg(e)
			draw()
		}
	}

	return resUser, resGood
}

func create(state *gamestate.State) (protocol.User, bool) {
	var iHelp, iUser, iPass, iPassConfirm, iLogin, iErr *screen.Item

	menu := screen.NewMenu(&state.Screen, screen.MakeItems([]screen.ItemSpec{
		{ &iHelp, 2, false },
		{ &iUser, 2, true },
		{ &iPass, 2, true },
		{ &iPassConfirm, 2, true },
		{ &iLogin, 1, true },
		{ &iErr, 3, false },
	}))
	menu.QueueFocus(iUser)

	helpMain := help{ "q: quit", true }
	helpText := help{ "Esc: leave textbox", false }

	loginErr := ""

	tUser := screen.NewTextbox(&menu, iUser, 5 + len("Username: "), false,
		screen.GraphRunes, consts.MaxUsernameLen)
	tPass := screen.NewTextbox(&menu, iPass, 5 + len("Password: "), true,
		screen.GraphRunes, consts.MaxPasswordLen)
	tPassConfirm := screen.NewTextbox(&menu, iPassConfirm,
		5 + len("Confirm Password: "), true, screen.GraphRunes, consts.MaxPasswordLen)

	draw := func() {
		state.Screen.Tc.Clear()

		state.DrawHeader()

		state.Screen.DrawHelp(0, iHelp.Start, state.Screen.Styles.Help, state.Screen.Styles.HelpBold,
			makeHelp(helpMain, helpText))
		state.Screen.DrawText(5, iUser.Start, state.Screen.Styles.Normal, "Username:")
		state.Screen.DrawText(5, iPass.Start, state.Screen.Styles.Normal, "Password:")
		state.Screen.DrawText(5, iPassConfirm.Start, state.Screen.Styles.Normal, "Confirm Password:")
		state.Screen.DrawText(5, iLogin.Start, state.Screen.Styles.Normal, "Login")
		state.Screen.DrawText(1, iErr.Start, state.Screen.Styles.Error, loginErr)
		tUser.Draw()
		tPass.Draw()
		tPassConfirm.Draw()
		menu.Draw()

		state.Screen.Tc.Show()
	}

	state.Screen.Tc.SetStyle(state.Screen.Styles.Normal)
	draw()

	loop := true
	resUser := protocol.User{}
	resGood := false
	for loop {
		e := pollMenu(&menu, state)

		switch e := e.(type) {
		case *tcell.EventKey:
			if e.Key() == tcell.KeyRune && e.Rune() == 'q' {
				return protocol.User{}, false
			}

		case *tcell.EventResize:
			draw()

		case *screen.Item:
			switch e {
			case iUser:
				helpMain.on, helpText.on = false, true
				draw()
				if doTextBox(&tUser, &menu, state, draw) {
					menu.QueueFocus(iPass)
				}
				helpMain.on, helpText.on = true, false

			case iPass:
				helpMain.on, helpText.on = false, true
				draw()
				if doTextBox(&tPass, &menu, state, draw) {
					menu.QueueFocus(iPassConfirm)
				}
				helpMain.on, helpText.on = true, false

			case iPassConfirm:
				helpMain.on, helpText.on = false, true
				draw()
				if doTextBox(&tPassConfirm, &menu, state, draw) {
					menu.QueueSelect(iLogin)
				}
				helpMain.on, helpText.on = true, false

			case iLogin:
				if len(tUser.Text) >= 1 && len(tPass.Text) >= 1 {
					if tPass.Text != tPassConfirm.Text {
						loginErr = "* Passwords do not match."
					} else {
						id := state.SendMsg(consts.ClientCreateAccount, tUser.Text, tPass.Text)
						username := tUser.Text
						state.Wait(func(msg protocol.Msg) {
							errstr, good := gamestate.CheckResponse(msg, id)
							if !good { return }

							if errstr == "" {
								resUser = protocol.User{ Name: username, Guest: false }
								resGood = true
								loop = false
							} else {
								if errstr == "exists" {
									loginErr = "* Username already taken."
								} else {
									state.Unwait()
									ErrMsgMenu(state, consts.ClientCreateAccount, errstr)
								}
							}
							state.Unwait()
						})
					}
				} else {
					if len(tUser.Text) < 1 {
						loginErr = "* Username must be at least 1 character."
					} else {
						loginErr = "* Password must be at least 8 characters."
					}
				}
			}
			draw()

		case protocol.Msg:
			failInvalidMsg(state, e)
			state.ApplyMsg(e)
			draw()
		}
	}

	return resUser, resGood
}

func guest(state *gamestate.State) (protocol.User, bool) {
	var iHelp, iUser, iLogin, iErr *screen.Item

	menu := screen.NewMenu(&state.Screen, screen.MakeItems([]screen.ItemSpec{
		{ &iHelp, 2, false },
		{ &iUser, 2, true },
		{ &iLogin, 1, true },
		{ &iErr, 3, false },
	}))
	menu.QueueFocus(iUser)

	helpMain := help{ "q: quit", true }
	helpText := help{ "Esc: leave textbox", false }

	loginErr := ""

	tUser := screen.NewTextbox(&menu, iUser, 5 + len("Username: "), false,
		screen.GraphRunes, consts.MaxUsernameLen)

	draw := func() {
		state.Screen.Tc.Clear()

		state.DrawHeader()

		state.Screen.DrawHelp(0, iHelp.Start, state.Screen.Styles.Help, state.Screen.Styles.HelpBold,
			makeHelp(helpMain, helpText))
		state.Screen.DrawText(5, iUser.Start, state.Screen.Styles.Normal, "Username:")
		state.Screen.DrawText(5, iLogin.Start, state.Screen.Styles.Normal, "Enter")
		state.Screen.DrawText(1, iErr.Start, state.Screen.Styles.Error, loginErr)
		tUser.Draw()
		menu.Draw()

		state.Screen.Tc.Show()
	}

	state.Screen.Tc.SetStyle(state.Screen.Styles.Normal)
	draw()

	loop := true
	resUser := protocol.User{}
	resGood := false
	for loop {
		e := pollMenu(&menu, state)

		switch e := e.(type) {
		case *tcell.EventKey:
			if e.Key() == tcell.KeyRune && e.Rune() == 'q' {
				return protocol.User{}, false
			}

		case *tcell.EventResize:
			draw()

		case *screen.Item:
			switch e {
			case iUser:
				helpMain.on, helpText.on = false, true
				draw()
				if doTextBox(&tUser, &menu, state, draw) {
					menu.QueueSelect(iLogin)
				}
				helpMain.on, helpText.on = true, false

			case iLogin:
				if len(tUser.Text) >= 1 {
					id := state.SendMsg(consts.ClientLogin, "guest", tUser.Text)
					username := tUser.Text
					state.Wait(func(msg protocol.Msg) {
						errstr, good := gamestate.CheckResponse(msg, id)
						if !good { return }

						if errstr == "" {
							resUser = protocol.User { Name: username, Guest: true }
							resGood = true
							loop = false
						} else {
							if errstr == "exists" {
								loginErr = "* Username already taken."
							} else {
								state.Unwait()
								ErrMsgMenu(state, consts.ClientLogin, errstr)
							}
						}
						state.Unwait()
					})
				} else {
					loginErr = "* Username must be at least 1 character."
				}
			}
			draw()

		case protocol.Msg:
			failInvalidMsg(state, e)
			state.ApplyMsg(e)
			draw()
		}
	}

	return resUser, resGood
}

func LoginMenu(state *gamestate.State) (protocol.User, bool) {
	var iHelp, iLogin, iGuest, iCreate, iAbout *screen.Item

	menu := screen.NewMenu(&state.Screen, screen.MakeItems([]screen.ItemSpec{
		{ &iHelp, 2, false },
		{ &iLogin, 2, true },
		{ &iGuest, 2, true },
		{ &iCreate, 2, true },
		{ &iAbout, 2, true },
	}))

	helpMain := help{ "q: quit", true }

	draw := func() {
		state.Screen.Tc.Clear()

		state.DrawHeader()

		state.Screen.DrawHelp(0, iHelp.Start, state.Screen.Styles.Help, state.Screen.Styles.HelpBold,
			makeHelp(helpMain))
		state.Screen.DrawText(5, iLogin.Start, state.Screen.Styles.Normal, "Login")
		state.Screen.DrawText(5, iGuest.Start, state.Screen.Styles.Normal, "Play as a Guest")
		state.Screen.DrawText(5, iCreate.Start, state.Screen.Styles.Normal, "Create Account")
		state.Screen.DrawText(5, iAbout.Start, state.Screen.Styles.Normal, "About")
		menu.Draw()

		state.Screen.Tc.Show()
	}

	state.Screen.Tc.SetStyle(state.Screen.Styles.Normal)
	draw()

	loop := true
	for loop {
		e := pollMenu(&menu, state)

		switch e := e.(type) {
		case *tcell.EventKey:
			if e.Key() == tcell.KeyRune && e.Rune() == 'q' {
				return protocol.User{}, false
			}

		case *tcell.EventResize:
			draw()

		case *screen.Item:
			switch e {
			case iLogin:
				user, good := login(state)
				if good {
					return user, true
				}
			case iGuest:
				user, good := guest(state)
				if good {
					return user, true
				}
			case iCreate:
				user, good := create(state)
				if good {
					return user, true
				}

			case iAbout:
				AboutMenu(state)
			}
			draw()
		}
	}

	return protocol.User{}, false
}
