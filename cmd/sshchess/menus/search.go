package menus

import  (
	"github.com/gdamore/tcell/v2"

	"codeberg.org/lunacb/sshchess/cmd/sshchess/gamestate"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/protocol"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/screen"
	"codeberg.org/lunacb/sshchess/lib/consts"
)

func search(state *gamestate.State, msg protocol.MsgSearch) {
	var iHelp, iTitle, iGap, iTrunc *screen.Item
	var iResults [pageSize]*screen.Item

	var items []screen.Item
	{
		spec := make([]screen.ItemSpec, 0, 11)
		spec = append(spec, screen.ItemSpec{ &iHelp, 2, false },
			screen.ItemSpec{ &iTitle, 2, false },
			screen.ItemSpec{ &iGap, 1, false })
		for i, _ := range iResults {
			spec = append(spec, screen.ItemSpec{ &iResults[i], 1, true })
		}
		spec = append(spec, screen.ItemSpec{ &iTrunc, 2, false })
		items = screen.MakeItems(spec)
	}
	menu := screen.NewMenu(&state.Screen, items)

	page := 0
	nPages := len(msg.Search) / pageSize
	if len(msg.Search) % pageSize != 0 || len(msg.Search) == 0 { nPages += 1 }

	helpMain := help{ "q: back, Enter: challenge user", true }
	helpNext := help{ "n: next", (nPages > 1) }
	helpPrev := help{ "p: prev", false }

	draw := func() {
		state.Screen.Tc.Clear()

		state.DrawHeader()

		state.Screen.DrawHelp(0, iHelp.Start, state.Screen.Styles.Help, state.Screen.Styles.HelpBold,
			makeHelp(helpMain))
		state.Screen.DrawText(5, iTitle.Start, state.Screen.Styles.Normal, "Results:")
		{
			list := msg.Search[page*pageSize:]
			if len(list) > pageSize { list = list[:pageSize] }

			table := screen.TableFromSearch(list)
			state.Screen.DrawTable(6, iGap.Start, state.Screen.Styles.Column,
				state.Screen.Styles.Normal, table, "Name", "Online", "Wins", "Losses")
			menu.Items = items[:len(list)]
		}
		if msg.Truncated {
			state.Screen.DrawText(5, iTrunc.Start + 1, state.Screen.Styles.Normal, "(truncated)")
		}

		menu.Draw()

		state.Screen.Tc.Show()
	}

	state.Screen.Tc.SetStyle(state.Screen.Styles.Normal)
	draw()

	loop := true
	for loop {
		e := pollMenu(&menu, state)

		switch e := e.(type) {
		case *tcell.EventKey:
			key := e.Key()
			if key == tcell.KeyRune {
				krune := e.Rune()
				if krune == 'q' {
					return
				}

				if krune == 'n' {
					if page + 1 < nPages {
						page += 1
						helpPrev.on = true
						if page + 1 == nPages {
							helpNext.on = false
						}
					}
					draw()
				} else if krune == 'p' {
					if page != 0 {
						page -= 1
						helpNext.on = true
						if page == 0 {
							helpPrev.on = false
						}
					}
					draw()
				}
			}

		case *tcell.EventResize:
			draw()

		case *screen.Item:
			for i, v := range iResults {
				if v == e {
					user := msg.Search[page*pageSize+i]
					if !user.Online {
						NotifyMenu(state, "That user is offline.")
					} else if user.Stat.Name == state.User.Name &&
							user.Stat.Guest == state.User.Guest {
						NotifyMenu(state, "You can't challenge yourself!")
					} else {
						var guestStr string
						if user.Stat.Guest {
							guestStr = "guest"
						} else {
							guestStr = "user"
						}

						waitmatchMenu(state, "Waiting for opponent...",
							consts.ClientChallengeUser, guestStr, user.Stat.Name)
					}
				}
			}
			draw()

		case protocol.Msg:
			failInvalidMsg(state, e)
			state.ApplyMsg(e)
			nPages = len(msg.Search) / pageSize
			if len(msg.Search) % pageSize != 0 || len(msg.Search) == 0 { nPages += 1 }
			if page >= nPages {page = nPages - 1 }
			helpNext.on = (page + 1 != nPages)
			helpPrev.on = (page != 0)
			draw()
		}
	}
}

func SearchMenu(state *gamestate.State) {
	var iHelp, iSearch, iOffline, iEnter *screen.Item

	menu := screen.NewMenu(&state.Screen, screen.MakeItems([]screen.ItemSpec{
		{ &iHelp, 2, false },
		{ &iSearch, 2, true },
		{ &iOffline, 2, true },
		{ &iEnter, 2, true },
	}))
	menu.QueueFocus(iSearch)

	helpMain := help{ "q: back", true }
	helpText := help{ "Esc: leave textbox", false }

	tSearch := screen.NewTextbox(&menu, iSearch, 5 + len("Search: "), false,
		screen.GraphRunes, consts.MaxUsernameLen)

	offline := false

	draw := func() {
		state.Screen.Tc.Clear()

		state.DrawHeader()

		state.Screen.DrawHelp(0, iHelp.Start, state.Screen.Styles.Help, state.Screen.Styles.HelpBold,
			makeHelp(helpMain, helpText))
		state.Screen.DrawText(5, iSearch.Start, state.Screen.Styles.Normal, "Search:")
		var offlineStr string
		if offline {
			offlineStr = "Yes"
		} else {
			offlineStr = "No"
		}
		state.Screen.DrawText(5, iOffline.Start,
			state.Screen.Styles.Normal, "Include Offline Users: ")
		state.Screen.DrawText(5 + len("Include Offline Users: "), iOffline.Start,
			state.Screen.Styles.Input, offlineStr)
		state.Screen.DrawText(5, iEnter.Start, state.Screen.Styles.Normal, "Enter")

		tSearch.Draw()

		menu.Draw()

		state.Screen.Tc.Show()
	}

	state.Screen.Tc.SetStyle(state.Screen.Styles.Normal)
	draw()

	loop := true
	for loop {
		e := pollMenu(&menu, state)

		switch e := e.(type) {
		case *tcell.EventKey:
			if e.Key() == tcell.KeyRune && e.Rune() == 'q' {
				return
			}

		case *tcell.EventResize:
			draw()

		case *screen.Item:
			switch e {
			case iSearch:
				helpMain.on, helpText.on = false, true
				draw()
				if doTextBox(&tSearch, &menu, state, draw) {
					menu.QueueSelect(iOffline)
				}
				helpMain.on, helpText.on = true, false

			case iOffline:
				offline = !offline

			case iEnter:
				var offlineStr string
				if offline {
					offlineStr = "true"
				} else {
					offlineStr = "false"
				}
				var id int
				if tSearch.Text == "" {
					id = state.SendMsg(consts.ClientSearchUser, offlineStr)
				} else {
					id = state.SendMsg(consts.ClientSearchUser, offlineStr, tSearch.Text)
				}

				gotOk := false
				state.Wait(func(msg protocol.Msg) {
					if !gotOk {
						errstr, good := gamestate.CheckResponse(msg, id)
						if !good { return }

						if errstr != "" {
							ErrMsgMenu(state, consts.ClientSearchUser, errstr)
						}

						gotOk = true
						return
					} else {
						switch msg := msg.(type) {
						case protocol.MsgSearch:
							state.Unwait()
							search(state, msg)
							menu.QueueSelect(iSearch)
						}
					}
				})
			}
			draw()

		case protocol.Msg:
			failInvalidMsg(state, e)
			state.ApplyMsg(e)
			draw()
		}
	}
}
