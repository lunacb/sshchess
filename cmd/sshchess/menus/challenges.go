package menus

import  (
	"github.com/gdamore/tcell/v2"

	"codeberg.org/lunacb/sshchess/cmd/sshchess/gamestate"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/protocol"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/screen"
	"codeberg.org/lunacb/sshchess/lib/consts"
)

func ChallengesMenu(state *gamestate.State) {
	var iHelp, iTitle, iGap *screen.Item
	var iChallenges [pageSize]*screen.Item

	var items []screen.Item
	{
		spec := make([]screen.ItemSpec, 0, 11)
		spec = append(spec, screen.ItemSpec{ &iHelp, 2, false },
			screen.ItemSpec{ &iTitle, 2, false },
			screen.ItemSpec{ &iGap, 1, false })
		for i, _ := range iChallenges {
			spec = append(spec, screen.ItemSpec{ &iChallenges[i], 1, true })
		}
		items = screen.MakeItems(spec)
	}
	menu := screen.NewMenu(&state.Screen, items)

	page := 0
	nPages := len(state.Challenges) / pageSize
	if len(state.Challenges) % pageSize != 0 || len(state.Challenges) == 0 { nPages += 1 }

	helpMain := help{ "q: back, Enter: accept challenge", true }
	helpNext := help{ "n: next", (nPages > 1) }
	helpPrev := help{ "p: prev", false }

	updatePages := func() {
		nPages = len(state.Challenges) / pageSize
		if len(state.Challenges) % pageSize != 0 || len(state.Challenges) == 0 { nPages += 1 }
		if page >= nPages {page = nPages - 1 }
		helpNext.on = (page + 1 != nPages)
		helpPrev.on = (page != 0)
	}

	draw := func() {
		state.Screen.Tc.Clear()

		state.DrawHeader()

		state.Screen.DrawHelp(0, iHelp.Start, state.Screen.Styles.Help, state.Screen.Styles.HelpBold,
			makeHelp(helpMain, helpNext, helpPrev))
		state.Screen.DrawText(5, iTitle.Start, state.Screen.Styles.Normal, "Challenges:")
		{
			list := state.Challenges[page*pageSize:]
			if len(list) > pageSize { list = list[:pageSize] }

			users := [][]protocol.User{}
			for _, v := range list {
				users = append(users, []protocol.User{v})
			}
			table := screen.TableFromUsers(users)
			state.Screen.DrawTable(6, iGap.Start,
				state.Screen.Styles.Column, state.Screen.Styles.Normal, table, "Name")
			menu.Items = items[:len(list)]
		}

		menu.Draw()

		state.Screen.Tc.Show()
	}

	state.Screen.Tc.SetStyle(state.Screen.Styles.Normal)
	draw()

	loop := true
	for loop {
		e := pollMenu(&menu, state)

		switch e := e.(type) {
		case *tcell.EventKey:
			key := e.Key()
			if key == tcell.KeyRune {
				krune := e.Rune()
				if krune == 'q' {
					return
				}

				if krune == 'n' {
					if page + 1 < nPages {
						page += 1
						helpPrev.on = true
						if page + 1 == nPages {
							helpNext.on = false
						}
					}
					draw()
				} else if krune == 'p' {
					if page != 0 {
						page -= 1
						helpNext.on = true
						if page == 0 {
							helpPrev.on = false
						}
					}
					draw()
				}
			}

		case *tcell.EventResize:
			draw()

		case *screen.Item:
			for i, v := range iChallenges {
				if v == e {
					user := state.Challenges[page*pageSize+i]
					var guestStr string
					if user.Guest {
						guestStr = "guest"
					} else {
						guestStr = "user"
					}

					wres := waitmatchMenu(state, "Waiting for opponent...",
						consts.ClientAcceptChallenge, guestStr, user.Name)

					if wres == WaitmatchQuit {
						NotifyMenu(state, "The user quit the game.")
					}

					// Remove the challenge entry if the game started successfully.
					if wres != WaitmatchCtx {
						for i := i + 1; i < len(state.Challenges); i += 1 {
							state.Challenges[i-1] = state.Challenges[i]
						}
						state.Challenges = state.Challenges[:len(state.Challenges)-1]
						updatePages()
					}

					break
				}
			}
			draw()

		case protocol.Msg:
			failInvalidMsg(state, e)
			state.ApplyMsg(e)
			updatePages()
			draw()
		}
	}
}
