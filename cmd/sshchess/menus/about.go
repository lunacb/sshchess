package menus

import  (
	"github.com/gdamore/tcell/v2"

	"codeberg.org/lunacb/sshchess/cmd/sshchess/gamestate"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/protocol"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/screen"
)

func AboutMenu(state *gamestate.State) {
	var iHelp, iInfo *screen.Item

	menu := screen.NewMenu(&state.Screen, screen.MakeItems([]screen.ItemSpec{
		{ &iHelp, 2, false },
		{ &iInfo, 7, false },
	}))

	helpMain := help{ "q: back", true }

	draw := func() {
		state.Screen.Tc.Clear()

		state.DrawHeader()

		state.Screen.DrawHelp(0, iHelp.Start, state.Screen.Styles.Help,
			state.Screen.Styles.HelpBold, makeHelp(helpMain))

			state.Screen.DrawText(5, iInfo.Start, state.Screen.Styles.Normal,
				"Copyright (c) 2022 lunacb <lunacb@disroot.org>" )
			state.Screen.DrawText(5, iInfo.Start + 2, state.Screen.Styles.Normal,
				"This program comes with ABSOLUTELY NO WARRANTY; See the GNU")
			state.Screen.DrawText(5, iInfo.Start + 3, state.Screen.Styles.Normal,
				"Genral Public License for more details.")
			state.Screen.DrawText(5, iInfo.Start + 5, state.Screen.Styles.Normal,
			"License and source code located at:")
			state.Screen.DrawText(5, iInfo.Start + 6, state.Screen.Styles.Normal,
			"        https://codeberg.org/lunacb/sshchess")

		menu.Draw()

		state.Screen.Tc.Show()
	}

	state.Screen.Tc.SetStyle(state.Screen.Styles.Normal)
	draw()

	loop := true
	for loop {
		e := pollMenu(&menu, state)

		switch e := e.(type) {
		case *tcell.EventKey:
			if e.Key() == tcell.KeyRune && e.Rune() == 'q' {
				loop = false
			}

		case *tcell.EventResize:
			draw()

		case protocol.Msg:
			failInvalidMsg(state, e)
			state.ApplyMsg(e)
		}
	}
}
