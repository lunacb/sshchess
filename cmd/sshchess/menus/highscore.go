package menus

import  (
	"github.com/gdamore/tcell/v2"

	"codeberg.org/lunacb/sshchess/cmd/sshchess/gamestate"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/protocol"
	"codeberg.org/lunacb/sshchess/cmd/sshchess/screen"
)

func HighscoreMenu(state *gamestate.State) {
	var iHelp, iToggle, iGap *screen.Item

	menu := screen.NewMenu(&state.Screen, screen.MakeItems([]screen.ItemSpec{
		{ &iHelp, 2, false },
		{ &iToggle, 2, false },
		{ &iGap, 2, false },
	}))

	helpMain := help{ "q: back, s: toggle alltime/weekly", true }

	weekly := false

	draw := func() {
		state.Screen.Tc.Clear()

		state.DrawHeader()

		state.Screen.DrawHelp(0, iHelp.Start, state.Screen.Styles.Help, state.Screen.Styles.HelpBold,
			makeHelp(helpMain))

		var weeklyStr string
		if weekly {
			weeklyStr = "Weekly"
		} else {
			weeklyStr = "All Time"
		}
		state.Screen.DrawText(5, iToggle.Start, state.Screen.Styles.Normal, "Scores: " + weeklyStr)
		{
			var list []protocol.UserStat
			if weekly {
				list = state.Weekly
			} else {
				list = state.Alltime
			}

			users := [][]protocol.UserStat{}
			for _, v := range list {
				users = append(users, []protocol.UserStat{v})
			}
			table := screen.TableFromUserStats(users)
			table = screen.TableWithLength(table, 3, 10)
			state.Screen.DrawTable(6, iGap.Start,
				state.Screen.Styles.Column, state.Screen.Styles.Normal, table, "Name", "Wins", "Losses")
		}

		menu.Draw()

		state.Screen.Tc.Show()
	}

	state.Screen.Tc.SetStyle(state.Screen.Styles.Normal)
	draw()

	loop := true
	for loop {
		e := pollMenu(&menu, state)

		switch e := e.(type) {
		case *tcell.EventKey:
			if e.Key() == tcell.KeyRune {
				krune := e.Rune()
				if krune == 'q' {
					return
				}

				if krune == 's' {
					weekly = !weekly
					draw()
				}
			}

		case *tcell.EventResize:
			draw()

		case protocol.Msg:
			failInvalidMsg(state, e)
			state.ApplyMsg(e)
			draw()
		}
	}
}
