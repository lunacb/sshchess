PREFIX=/usr/local
MANPREFIX=${PREFIX}/share/man/man1

install: install-client install-server install-docs

uninstall:
	rm ${MANPREFIX}/sshchess.1
	rm ${MANPREFIX}/sshchessd.1
	rm ${PREFIX}/bin/sshchess
	rm ${PREFIX}/bin/sshchessd

install-client:
	GOBIN=${PREFIX}/bin go install ./cmd/sshchess

install-server:
	GOBIN=${PREFIX}/bin go install ./cmd/sshchessd

install-docs:
	mkdir -p ${MANPREFIX}
	install -m 644 ./doc/sshchess.1 ${MANPREFIX}
	install -m 644 ./doc/sshchessd.1 ${MANPREFIX}
