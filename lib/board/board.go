package board

import (
	"strconv"
	"strings"
)

type PieceName int

const (
	PKing PieceName = iota
	PQueen
	PRook
	PBishop
	PKnight
	PPawn
	PNone
	PIgnore
)

type MoveType int

const (
	MoveUp MoveType = 1 << iota
	MoveDown
	MoveLeft
	MoveRight
	MoveForwards
)

type MoveCastle struct {
	Move MoveType // left or right
}

type MovePiece struct {
	 X, Y int
	 Piece interface{}
}

func (m MovePiece) Log() (string, bool) {
	return "", false
}

type MoveKing struct {
	Move MoveType // one or two of up, down, left, or right
}

type MoveQueen struct {
	Move MoveType // one or two of up, down, left, or right
	Length int
}

type MoveRook struct {
	Move MoveType // one of up, down, left, or right
	Length int
}

type MoveBishop struct {
	Move MoveType // two of up, down, left, or right
	Length int
}

type MoveKnight struct {
	Two MoveType // one of up, down, left, or right
	One MoveType // one of up, down, left, or right
}

type MovePawn struct {
	// Relative to one space forwards from the current position.
	Move MoveType // one of forwards, left, right, or 0
	Promote PieceName
}


type WinType int
// bool so we can do an easy `!side` to the the opposing side.
type GameSide bool
type Piece struct {
	Piece PieceName
	Side GameSide
	Moved bool
}

type MoveResults struct {
	x1, y1, x2, y2 int
	promote PieceName
	has_second bool
	sx1, sy1, sx2, sy2 int
}

const (
	WinNone WinType = iota
	WinCheckmate
	WinStalemate
)

const (
	TWhite GameSide = true
	TBlack GameSide = false
)

type Board struct {
	Board [8][8]Piece
}

func strPieceDir(m MoveType) string {
	switch m {
	case MoveUp | MoveLeft: return "upleft"
	case MoveDown | MoveLeft: return "downleft"
	case MoveUp | MoveRight: return "upright"
	case MoveDown | MoveRight: return "downright"
	case MoveUp: return "up"
	case MoveDown: return "down"
	case MoveLeft: return "left"
	case MoveRight: return "right"
	case MoveForwards: return "forwards"
	default: return "none"
	}

	return "none"
}

func (m MovePiece) MakeMsg() string  {
	msg := []string{strconv.Itoa(m.X), strconv.Itoa(m.Y)}
	switch piece := m.Piece.(type) {
		case MoveKing:
			msg = append(msg, "king", strPieceDir(piece.Move))
		case MoveQueen:
			msg = append(msg, "queen", strPieceDir(piece.Move), strconv.Itoa(piece.Length))
		case MoveRook:
			msg = append(msg, "rook", strPieceDir(piece.Move), strconv.Itoa(piece.Length))
		case MoveCastle:
			msg = append(msg, "castle", strPieceDir(piece.Move))
		case MoveBishop:
			msg = append(msg, "bishop", strPieceDir(piece.Move), strconv.Itoa(piece.Length))
		case MoveKnight:
			msg = append(msg, "knight", strPieceDir(piece.Two), strPieceDir(piece.One))
		case MovePawn:
			msg = append(msg, "pawn", strPieceDir(piece.Move), PieceNameString(piece.Promote))
	}

	return strings.Join(msg, " ")
}

func PieceNameString(p PieceName) string {
	switch p {
	case PKing: return "king"
	case PQueen: return "queen"
	case PRook: return "rook"
	case PBishop: return "bishop"
	case PKnight: return "knight"
	case PPawn: return "pawn"
	default: return "none"
	}
	return "none"
}

func NewBoard() Board {
	// Some "macros" so we can visually represent the entire chessboard in
	// under 80 characters of length.

	const (
		K = PKing
		Q = PQueen
		r = PRook
		b = PBishop
		k = PKnight
		p = PPawn
	)
	W := func(p PieceName) Piece {
		return Piece{Piece: p, Side: TWhite, Moved: false}
	}
	B := func(p PieceName) Piece {
		return Piece{Piece: p, Side: TBlack, Moved: false}
	}
	N := Piece{Piece: PNone, Side: TWhite, Moved: false}

	return Board{
		Board: [8][8]Piece{
			{B(r), B(k), B(b), B(Q), B(K), B(b), B(k), B(r)},
			{B(p), B(p), B(p), B(p), B(p), B(p), B(p), B(p)},
			{N,    N,    N,    N,    N,    N,    N,    N   },
			{N,    N,    N,    N,    N,    N,    N,    N   },
			{N,    N,    N,    N,    N,    N,    N,    N   },
			{N,    N,    N,    N,    N,    N,    N,    N   },
			{W(p), W(p), W(p), W(p), W(p), W(p), W(p), W(p)},
			{W(r), W(k), W(b), W(Q), W(K), W(b), W(k), W(r)},
		},
	}
}

func EmptyBoard() Board {
	pieces := [8][8]Piece{}
	for i, v := range pieces {
		for j, _ := range v {
			pieces[i][j] = Piece{
				Piece: PNone,
				Side: TWhite,
				Moved: false,
			}
		}
	}

	return Board{ Board: pieces }
}

func moveDir(side GameSide, move MoveType) (x int, y int) {
	x = 0
	y = 0
	if move & MoveUp != 0 {
		x -= 1
	}
	if move & MoveDown != 0 {
		x += 1
	}
	if move & MoveLeft != 0 {
		y -= 1
	}
	if move & MoveRight != 0 {
		y += 1
	}
	if move & MoveForwards != 0 {
		if side == TWhite {
			x -= 1
		} else {
			x += 1
		}
	}

	return x, y
}

func posOnBoard(x int, y int) bool {
	return (x >= 0 && x <= 7 && y >= 0 && y <= 7)
}

func (b Board) Msg() string {
	strs := []string{}
	for y, v := range b.Board {
		for x, v := range v {
			if v.Piece != PNone {
				// TODO: use common function for this
				var color string
				if v.Side == TWhite {
					color = "white"
				} else {
					color = "black"
				}
				strs = append(strs, strconv.Itoa(x + 1), strconv.Itoa(9 - (y + 1)),
					PieceNameString(v.Piece), color)
			}
		}
	}

	return strings.Join(strs, " ")
}

func (b Board) clearPath(x int, y int, dx int, dy int, length int) bool {
	for i := 0; i < length - 1; i++ {
		x += dx
		y += dy

		if b.Board[x][y].Piece != PNone { return false }
	}

	return true
}

func (b Board) forEachMove(x int, y int, each func(move MovePiece) bool) bool {
	var set []interface{}

	mUp := MoveUp
	mDown := MoveDown
	mLeft := MoveLeft
	mRight := MoveRight
	type mKing = MoveKing
	type mQueen = MoveQueen
	type mRook = MoveRook
	type mBishop = MoveBishop
	type mKnight = MoveKnight
	type mPawn = MovePawn
	type mCastle = MoveCastle

	switch b.Board[x][y].Piece {
	case PKing:
		set = []interface{}{
			mKing{mUp}, mKing{mDown}, mKing{mLeft}, mKing{mRight},
			mCastle{mLeft}, mCastle{mRight},
		}
	case PQueen:
		set = []interface{}{
			mQueen{mUp, 1}, mQueen{mDown, 1}, mQueen{mLeft, 1}, mQueen{mRight, 1},
			mQueen{mUp, 2}, mQueen{mDown, 2}, mQueen{mLeft, 2}, mQueen{mRight, 2},
			mQueen{mUp, 3}, mQueen{mDown, 3}, mQueen{mLeft, 3}, mQueen{mRight, 3},
			mQueen{mUp, 4}, mQueen{mDown, 4}, mQueen{mLeft, 4}, mQueen{mRight, 4},
			mQueen{mUp, 5}, mQueen{mDown, 5}, mQueen{mLeft, 5}, mQueen{mRight, 5},
			mQueen{mUp, 6}, mQueen{mDown, 6}, mQueen{mLeft, 6}, mQueen{mRight, 6},
			mQueen{mUp, 7}, mQueen{mDown, 7}, mQueen{mLeft, 7}, mQueen{mRight, 7},
			mQueen{mUp, 8}, mQueen{mDown, 8}, mQueen{mLeft, 8}, mQueen{mRight, 8},
			mQueen{mUp | mLeft, 1}, mQueen{mUp | mRight, 1}, mQueen{mDown | mLeft, 1},
			mQueen{mDown | mRight, 1},
			mQueen{mUp | mLeft, 2}, mQueen{mUp | mRight, 2}, mQueen{mDown | mLeft, 2},
			mQueen{mDown | mRight, 2},
			mQueen{mUp | mLeft, 3}, mQueen{mUp | mRight, 3}, mQueen{mDown | mLeft, 3},
			mQueen{mDown | mRight, 3},
			mQueen{mUp | mLeft, 4}, mQueen{mUp | mRight, 4}, mQueen{mDown | mLeft, 4},
			mQueen{mDown | mRight, 4},
			mQueen{mUp | mLeft, 5}, mQueen{mUp | mRight, 5}, mQueen{mDown | mLeft, 5},
			mQueen{mDown | mRight, 5},
			mQueen{mUp | mLeft, 6}, mQueen{mUp | mRight, 6}, mQueen{mDown | mLeft, 6},
			mQueen{mDown | mRight, 6},
			mQueen{mUp | mLeft, 7}, mQueen{mUp | mRight, 7}, mQueen{mDown | mLeft, 7},
			mQueen{mDown | mRight, 7},
			mQueen{mUp | mLeft, 8}, mQueen{mUp | mRight, 8}, mQueen{mDown | mLeft, 8},
			mQueen{mDown | mRight, 8},
		}

	case PRook:
		set = []interface{}{
			mRook{mUp, 1}, mRook{mDown, 1}, mRook{mLeft, 1}, mRook{mRight, 1},
			mRook{mUp, 2}, mRook{mDown, 2}, mRook{mLeft, 2}, mRook{mRight, 2},
			mRook{mUp, 3}, mRook{mDown, 3}, mRook{mLeft, 3}, mRook{mRight, 3},
			mRook{mUp, 4}, mRook{mDown, 4}, mRook{mLeft, 4}, mRook{mRight, 4},
			mRook{mUp, 5}, mRook{mDown, 5}, mRook{mLeft, 5}, mRook{mRight, 5},
			mRook{mUp, 6}, mRook{mDown, 6}, mRook{mLeft, 6}, mRook{mRight, 6},
			mRook{mUp, 7}, mRook{mDown, 7}, mRook{mLeft, 7}, mRook{mRight, 7},
			mRook{mUp, 8}, mRook{mDown, 8}, mRook{mLeft, 8}, mRook{mRight, 8},
		}

	case PBishop:
		set = []interface{}{
			mBishop{mUp | mLeft, 1}, mBishop{mUp | mRight, 1}, mBishop{mDown | mLeft, 1},
			mBishop{mDown | mRight, 1},
			mBishop{mUp | mLeft, 2}, mBishop{mUp | mRight, 2}, mBishop{mDown | mLeft, 2},
			mBishop{mDown | mRight, 2},
			mBishop{mUp | mLeft, 3}, mBishop{mUp | mRight, 3}, mBishop{mDown | mLeft, 3},
			mBishop{mDown | mRight, 3},
			mBishop{mUp | mLeft, 4}, mBishop{mUp | mRight, 4}, mBishop{mDown | mLeft, 4},
			mBishop{mDown | mRight, 4},
			mBishop{mUp | mLeft, 5}, mBishop{mUp | mRight, 5}, mBishop{mDown | mLeft, 5},
			mBishop{mDown | mRight, 5},
			mBishop{mUp | mLeft, 6}, mBishop{mUp | mRight, 6}, mBishop{mDown | mLeft, 6},
			mBishop{mDown | mRight, 6},
			mBishop{mUp | mLeft, 7}, mBishop{mUp | mRight, 7}, mBishop{mDown | mLeft, 7},
			mBishop{mDown | mRight, 7},
			mBishop{mUp | mLeft, 8}, mBishop{mUp | mRight, 8}, mBishop{mDown | mLeft, 8},
			mBishop{mDown | mRight, 8},
		}

	case PKnight:
		set = []interface{}{
			mKnight{mUp, mLeft}, mKnight{mUp, mRight}, mKnight{mDown, mLeft}, mKnight{mDown, mRight},
			mKnight{mLeft, mUp}, mKnight{mLeft, mDown}, mKnight{mRight, mUp}, mKnight{mRight, mDown},
		}

	case PPawn:
		set = []interface{}{
			mPawn{0, PIgnore}, mPawn{MoveForwards, PIgnore},
			mPawn{mLeft, PIgnore}, mPawn{mRight, PIgnore},
		}

	case PNone:
		return false
	}

	for _, v := range set {
		p := MovePiece{
			X: x,
			Y: y,
			Piece: v,
		}
		if each(p) { return true }
	}

	return false
}

func (b Board) FindMoveFromPos(side GameSide, sx, sy, dx, dy int) (MovePiece, bool) {
	if !posOnBoard(sx, sy) || b.Board[sx][sy].Piece == PNone {
		return MovePiece{}, false
	}

	res := MovePiece{}
	found := b.forEachMove(sx, sy, func(move MovePiece) bool {
		switch piece := move.Piece.(type) {
		case MoveKing:
			dir_x, dir_y := moveDir(side, piece.Move)
			if dx == move.X + dir_x && dy == move.Y + dir_y {
				res = move
				return true
			}

		case MoveQueen:
			dir_x, dir_y := moveDir(side, piece.Move)
			if dx == move.X + dir_x * piece.Length && dy == move.Y + dir_y * piece.Length {
				res = move
				return true
			}

		case MoveRook:
			dir_x, dir_y := moveDir(side, piece.Move)
			if dx == move.X + dir_x * piece.Length && dy == move.Y + dir_y * piece.Length {
				res = move
				return true
			}

		case MoveBishop:
			dir_x, dir_y := moveDir(side, piece.Move)
			if dx == move.X + dir_x * piece.Length && dy == move.Y + dir_y * piece.Length {
				res = move
				return true
			}

		case MoveKnight:
			dir_x_two, dir_y_two := moveDir(side, piece.Two)
			dir_x_one, dir_y_one := moveDir(side, piece.One)
			x, y := move.X + dir_x_two * 2, move.Y + dir_y_two * 2
			x, y = x + dir_x_one * 1, y + dir_y_one * 1
			if dx == x && dy == y {
				res = move
				return true
			}

		case MovePawn:
			dir_x, dir_y := moveDir(side, piece.Move)
			length := 1
			if dir_x != 0 {
				length = 2
			} else {
				dir_x, _ = moveDir(side, MoveForwards)
			}
			if dx == move.X + dir_x * length && dy == move.Y + dir_y * length {
				res = move
				return true
			}

		case MoveCastle:
			if piece.Move == MoveLeft {
				if dx == move.X && dy == 2 {
					res = move
					return true
				}
			} else {
				if dx == move.X && dy == 6 {
					res = move
					return true
				}
			}
		}

		return false
	})

	return res, found
}

func (b Board) stalemated(side GameSide) bool {
	for i, v := range b.Board {
		for j, v :=  range v {
			if v.Side != side || v.Piece == PNone { continue }
			if b.forEachMove(i, j, func(move MovePiece) bool {
				_, danger, good := b.moveValid(side, move, true)
				return (good && !danger)
			}) {
				return false
			}
		}
	}

	return true
}

func (b Board) KingDanger(side GameSide) bool {
	for i, v := range b.Board {
		for j, v := range v {
			if v.Side == side || v.Piece == PNone { continue }
			if b.forEachMove(i, j, func(move MovePiece) bool {
				pos, _, good := b.moveValid(!side, move, false)
				return (good && b.Board[pos.x2][pos.y2].Side == side && b.Board[pos.x2][pos.y2].Piece == PKing)
			}) {
				return true
			}
		}
	}

	return false
}

func (b Board) Checkmated(side GameSide) WinType {
	if b.stalemated(side) {
		if b.KingDanger(side) {
			return WinCheckmate
		} else {
			return WinStalemate
		}
	}

	return WinNone
}

func promotable(side GameSide, x int) bool {
	if side == TWhite {
		return (x == 0)
	} else {
		return (x == 7)
	}
}

func (b Board) moveValid(side GameSide, move MovePiece, checkKing bool) (pos MoveResults, danger bool, good bool) {
	pos = MoveResults{move.X, move.Y, 0, 0, PNone, false, 0, 0, 0, 0}
	can_capture := true
	must_capture := false

	move_piece := PNone
	switch move.Piece.(type) {
	case MoveKing, MoveCastle: move_piece = PKing
	case MoveQueen: move_piece = PQueen
	case MoveRook: move_piece = PRook
	case MoveBishop: move_piece = PBishop
	case MoveKnight: move_piece = PKnight
	case MovePawn: move_piece = PPawn
	}
	if b.Board[move.X][move.Y].Piece != move_piece { return MoveResults{}, false, false }

	switch piece := move.Piece.(type) {
	case MoveKing:
		dir_x, dir_y := moveDir(side, piece.Move)
		pos.x2, pos.y2 = move.X + dir_x, move.Y + dir_y
		if !posOnBoard(pos.x2, pos.y2) { return MoveResults{}, false, false }

	case MoveQueen:
		dir_x, dir_y := moveDir(side, piece.Move)
		pos.x2, pos.y2 = move.X + dir_x * piece.Length, move.Y + dir_y * piece.Length
		if !posOnBoard(pos.x2, pos.y2) { return MoveResults{}, false, false }
		if !b.clearPath(move.X, move.Y, dir_x, dir_y, piece.Length) { return MoveResults{}, false, false }

	case MoveRook:
		dir_x, dir_y := moveDir(side, piece.Move)
		pos.x2, pos.y2 = move.X + dir_x * piece.Length, move.Y + dir_y * piece.Length
		if !posOnBoard(pos.x2, pos.y2) { return MoveResults{}, false, false }
		if !b.clearPath(move.X, move.Y, dir_x, dir_y, piece.Length) { return MoveResults{}, false, false }

	case MoveBishop:
		dir_x, dir_y := moveDir(side, piece.Move)
		pos.x2, pos.y2 = move.X + dir_x * piece.Length, move.Y + dir_y * piece.Length
		if !posOnBoard(pos.x2, pos.y2) { return MoveResults{}, false, false }
		if !b.clearPath(move.X, move.Y, dir_x, dir_y, piece.Length) { return MoveResults{}, false, false }

	case MoveKnight:
		dir_x_two, dir_y_two := moveDir(side, piece.Two)
		dir_x_one, dir_y_one := moveDir(side, piece.One)
		pos.x2, pos.y2 = move.X + dir_x_two * 2, move.Y + dir_y_two * 2
		pos.x2, pos.y2 = pos.x2 + dir_x_one * 1, pos.y2 + dir_y_one * 1
		if !posOnBoard(pos.x2, pos.y2) { return MoveResults{}, false, false }

	case MovePawn:
		dir_x, dir_y := moveDir(side, piece.Move)
		length := 1
		if dir_x != 0 {
			if(dir_y != 0) { panic("dir_y != 0") }
			length = 2
			if(!b.clearPath(move.X, move.Y, dir_x, 0, length)) { return MoveResults{}, false, false }
		} else {
			dir_x, _ = moveDir(side, MoveForwards)
		}
		pos.x2, pos.y2 = move.X + dir_x * length, move.Y + dir_y * length
		if !posOnBoard(pos.x2, pos.y2) { return MoveResults{}, false, false }
		if length == 2 && b.Board[move.X][move.Y].Moved { return MoveResults{}, false, false }
		if dir_y == 0 {
			can_capture = false
		} else {
			must_capture = true
		}

		if piece.Promote != PIgnore && promotable(side, pos.x2) != (piece.Promote != PNone) {
			return MoveResults{}, false, false
		} else {
			pos.promote = piece.Promote
		}

	case MoveCastle:
		if b.Board[move.X][move.Y].Moved || move.Y != 4 {
			return MoveResults{}, false, false
		}

		if piece.Move == MoveLeft {

		} else {

		}

		if piece.Move == MoveLeft {
			if b.Board[move.X][0].Piece != PRook ||
					b.Board[move.X][0].Moved ||
					b.Board[move.X][3].Piece != PNone ||
					b.Board[move.X][2].Piece != PNone ||
					b.Board[move.X][1].Piece != PNone {
				return MoveResults{}, false, false
			}

			pos.x2 = move.X
			pos.y2 = 2
			pos.has_second = true
			pos.sx1 = move.X
			pos.sy1 = 0
			pos.sx2 = move.X
			pos.sy2 = 3
		} else {
			if b.Board[move.X][7].Piece != PRook ||
					b.Board[move.X][7].Moved ||
					b.Board[move.X][5].Piece != PNone ||
					b.Board[move.X][6].Piece != PNone {
				return MoveResults{}, false, false
			}

			pos.x2 = move.X
			pos.y2 = 6
			pos.has_second = true
			pos.sx1 = move.X
			pos.sy1 = 7
			pos.sx2 = move.X
			pos.sy2 = 5
		}
	}

	if b.Board[pos.x2][pos.y2].Piece != PNone {
		if !can_capture || b.Board[pos.x2][pos.y2].Side == side {
			return MoveResults{}, false, false
		}
	} else if must_capture {
		return MoveResults{}, false, false
	}

	// These will be reset at the end of the function because it doesn't take a
	// pointer.
	b.Board[pos.x2][pos.y2] = b.Board[move.X][move.Y]
	b.Board[pos.x2][pos.y2].Moved = true
	if pos.promote != PNone && pos.promote != PIgnore {
		b.Board[pos.x2][pos.y2].Piece = pos.promote
	}
	b.Board[move.X][move.Y].Piece = PNone
	if pos.has_second {
		b.Board[pos.sx2][pos.sy2] = b.Board[pos.sx1][pos.sy1]
		b.Board[pos.sx2][pos.sy2].Moved = true
		b.Board[pos.sx1][pos.sy1].Piece = PNone
	}

	if checkKing && b.KingDanger(side) {return MoveResults{}, true, false }

	return pos, false, true
}

func (b Board) MoveValid(side GameSide, move MovePiece) (MoveResults, bool) {
	res, _, good := b.moveValid(side, move, true)
	return res, good
}

func (b *Board) Move(pos MoveResults) {
	b.Board[pos.x2][pos.y2] = b.Board[pos.x1][pos.y1]
	b.Board[pos.x2][pos.y2].Moved = true
	if pos.promote != PNone && pos.promote != PIgnore {
		b.Board[pos.x2][pos.y2].Piece = pos.promote
	}
	b.Board[pos.x1][pos.y1].Piece = PNone
	if pos.has_second {
		b.Board[pos.sx2][pos.sy2] = b.Board[pos.sx1][pos.sy1]
		b.Board[pos.sx2][pos.sy2].Moved = true
		b.Board[pos.sx1][pos.sy1].Piece = PNone
	}
}

// Returns a string that can be used as a protocol msg from the server's side.
func (b *Board) TryMove(side GameSide, move MovePiece) (string, bool) {
	pos, good := b.MoveValid(side, move)
	if !good { return "", false }
	b.Move(pos)

	strs := []string{
		strconv.Itoa(pos.x1), strconv.Itoa(pos.y1),
		strconv.Itoa(pos.x2), strconv.Itoa(pos.y2),
		PieceNameString(pos.promote),
	}
	if pos.has_second {
		strs = append(strs, strconv.Itoa(pos.sx1),
			strconv.Itoa(pos.sy1), strconv.Itoa(pos.sx2),
			strconv.Itoa(pos.sy2), "none")
	}

	return strings.Join(strs, " "), true
}

func (b *Board) MoveDirty(sx, sy, dx, dy int, promote PieceName) {
	b.Board[dx][dy] = b.Board[sx][sy]
	b.Board[dx][dy].Moved = true
	if promote != PNone {
		b.Board[dx][dy].Piece = promote
	}
	b.Board[sx][sy].Piece = PNone
}

func PawnNeedsPromote(side GameSide, x int) bool {
	if x == 0 {
		return (side == TWhite)
	} else if x == 7 {
		return (side == TBlack)
	}

	return false
}
