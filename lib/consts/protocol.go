package consts

import (
)

const (
	ClientSendVersion = "version"

	ClientLogin = "login"
	ClientTerm = "bye"

	ClientCreateAccount = "create"
	// Enter waiting mode.
	ClientRandomMatch = "match"
	ClientChallengeUser = "challenge"
	ClientAcceptChallenge = "accept"
	// Enter game mode.
	ClientWatchMatch = "watch"
	ClientSearchUser = "search"

	ClientWaitingQuit = "wait.quit"

	// Must be sent on your turn. Changes to opponents turn.
	// If promoting a pawn, the piece to be promoted to must be specified.
	ClientGameMove = "game.move"
	// Enter waiting mode.
	ClientGameRematch = "game.rematch"
	// Forfeit or quit after a completed match.
	ClientGameQuit =  "game.quit"

	ClientWatchQuit = "watch.quit"

	ServerVersion = "version"

	ServerOk = "ok"
	ServerErr = "err"

	ServerTerm = "bye"
	ServerAddChallenge = "challenge"
	ServerRmChallenge = "rmchallenge"
	ServerAddMatch = "match"
	ServerRmMatch = "rmmatch"

	ServerSearch = "search"

	ServerLeaderboard = "leaderboard"

	ServerWaitingQuit = "wait.quit"

	// The game has begun.
	ServerGameBegin = "game.begin"
	// The opponent left.
	ServerGameAlone = "game.alone"
	// It's now the client's turn.
	ServerGameTurn = "game.turn"
	// A lis of starting and ending positions for pieces to be moved between.
	// The ending position can specify that the piece has been captured.
	ServerGameMove = "game.move"
	// Says which player won.
	ServerGameEnd = "game.end"
	// The other player  wants a rematch.
	ServerGameWantRematch = "game.rematch"

	ServerWatchBegin = "watch.begin"
	ServerWatchBoard = "watch.board"
	ServerWatchTurn = "watch.turn"
	ServerWatchMove = "watch.move"
	ServerWatchEnd = "watch.end"
	ServerWatchDestroy = "watch.bye"

	MaxUsernameLen = 32
	MaxPasswordLen = 32

	// all ascii printable characters except space
	NameCharStart = '!'
	NameCharEnd = '~'
)
