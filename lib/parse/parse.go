package parse

import (
	"strconv"

	"codeberg.org/lunacb/sshchess/lib/board"
	"codeberg.org/lunacb/sshchess/lib/consts"
)

func GetBool(str string) (res bool, good bool) {
	if str == "true" {
		return true, true
	} else if str == "false" {
		return false, true
	} else {
		return false, false
	}
}

func GetGuest(str string) (res bool, good bool) {
	if str == "guest" {
		return true, true
	} else if str == "user" {
		return false, true
	} else {
		return false, false
	}
}

func GetColor(str string) (res board.GameSide, good bool) {
	if str == "white" {
		return board.TWhite, true
	} else if str == "black" {
		return board.TBlack, true
	} else {
		return board.TWhite, false
	}
}

func GetAlltime(str string) (res bool, good bool) {
	if str == "alltime" {
		return true, true
	} else if str == "weekly" {
		return false, true
	} else {
		return false, false
	}
}

func GetBoardPos(xstr string, ystr string) (x int, y int, good bool) {
	var err error
	if x, err = strconv.Atoi(xstr); err != nil {
		return 0, 0, false
	}
	if y, err = strconv.Atoi(ystr); err != nil {
		return 0, 0, false
	}
	if x < 0 || x > 7 { return 0, 0, false }
	if y < 0 || y > 7 { return 0, 0, false }

	return x, y, true
}

func GetPiece(str string) (board.PieceName, bool) {
	switch str {
	case "king": return board.PKing, true
	case "queen": return board.PQueen, true
	case "rook": return board.PRook, true
	case "bishop": return board.PBishop, true
	case "knight": return board.PKnight, true
	case "pawn": return board.PPawn, true
	case "none": return board.PNone, true
	default:
	}

	return 0, false
}

func GetPromote(str string) (board.PieceName, bool) {
	switch str {
	case "queen": return board.PQueen, true
	case "rook": return board.PRook, true
	case "bishop": return board.PBishop, true
	case "knight": return board.PKnight, true
	case "none": return board.PNone, true
	default:
	}

	return 0, false
}

func UsernameGood(str string) bool {
	if len(str) < 1 || len(str) > consts.MaxUsernameLen { return false }
	for _, v := range []rune(str) {
		if v < consts.NameCharStart || v > consts.NameCharEnd {
			return false
		}
	}

	return true
}

func PasswordGood(str string) bool {
	if len(str) < 1 || len(str) > consts.MaxPasswordLen { return false }
	for _, v := range []rune(str) {
		if v < consts.NameCharStart || v > consts.NameCharEnd {
			return false
		}
	}

	return true
}

func DisplayUser(name string, guest bool) string {
	if guest {
		return name + " (guest)"
	} else {
		return name
	}
}
